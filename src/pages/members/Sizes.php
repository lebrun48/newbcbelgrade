<?php

class Sizes extends ConfigTable
{

  protected function buildTableDef()
  {
    $this->colDefinition = [
        "index" => [
            COL_HIDDEN  => true,
            COL_PRIMARY => true,
        ],
        "id"    => [
            COL_TITLE => "id"
        ],
        "name"  => [
            COL_TITLE      => "Nom",
            COL_TD_ACTIONS => true,
        ],
    ];
    if (!utils()->hasRootRole()) {
      unset($this->colDefinition["id"]);
    }
    dbUtil()->tables["configTab"] = [
        DB_COLS_DEFINITION => $this->colDefinition,
        DB_RIGHTS_ACCES    => TABLE_RIGHT_EDIT | TABLE_RIGHT_DELETE | TABLE_RIGHT_USER_ACTION
    ];
  }

  function __construct()
  {
    $this->buildTableDef();

    parent::__construct("configTab");
    $this->buildFullTable();
  }

  protected function getConfigObject(): ConfigBase
  {
    return ConfigProject::get(CONFIG_MEMBERS_SIZES);
  }

  protected function getConfigRow()
  {
    return [
        "id"   => $_REQUEST["id"],
        "name" => $_REQUEST["name"],
    ];
  }
}
