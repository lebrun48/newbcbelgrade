<?php

class SizesEdit
{

  public function __construct()
  {
    utils()->action == "update" && $row = ConfigProject::get(CONFIG_MEMBERS_SIZES)->getByIndex($_REQUEST["primary"][0]);

    $fields = [
        "!name" => [
            ED_LABEL       => "Nom",
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_VALIDATE    => [
                ED_VALIDATE_REQUIRED   => true,
                ED_VALIDATE_MAX_LENGTH => 50,
                ED_VALIDATE_INVALIDE   => "Manquant"
            ],
            ED_FIELD_WIDTH => 12,
        ],
        "!id"   => [ED_TYPE => ED_TYPE_HIDDEN]
    ];

    msgBox(BuildForm::getForm($fields, $row), "Edition function", null, [MSGBOX_BUTTON_ACTION => "Enregistrer", MSGBOX_BUTTON_CLOSE => "Annuler"]);
  }

}

/*{"1":{"id":"1","short":"SEN_N1_H","name":"S\u00e9niors 1er Nationale Hommes","disabled":"1","ageMin":"16","ageMax":null,"code":"1"},
 */