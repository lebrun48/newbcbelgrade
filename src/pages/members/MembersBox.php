<?php

class MembersBox
{

  function __construct()
  {
    switch ($_REQUEST["page"]) {
      case "configTab":
        new ConfigTabBox;
        break;

      default:
        echo "<div id=membersBox>";
        $table = new Members;
        if (!($table->withCell = $_REQUEST["withCells"] && $_REQUEST["active"] != "sport" || $_REQUEST["active"] == "coti")) {
          $table->colDefinition["cellRef"][COL_HIDDEN] = true;
          $list = ConfigProject::get()->getFunctionsForRole([ROLE_PLAYER, ROLE_COACH]);
          foreach ($list as $function) {
            $table->defaultWhere .= "functions like '%-$function-%' or ";
          }
          $table->defaultWhere = "($table->defaultWhere" . substr($table->defaultWhere, 0, -4) . ')';
        }

        $table->setDefaultSort("functions");
        $table->otherSort = "name";
        $table->settings = [TABLE_ATTR_KEEP_MULTI_SELECTION => true];
        $table->buildFullTable();

        utils()->insertReadyFunction("tableListener", "members");
        echo "</div>";
    }
  }

}
