<?php

class Categories extends ConfigTable
{

  protected function buildTableDef()
  {
    $this->colDefinition = [
        "index"  => [
            COL_HIDDEN  => true,
            COL_PRIMARY => true,
        ],
        "id"     => [
            COL_TITLE => "id",
        ],
        "short"  => [
            COL_TITLE      => "Nom abrégé",
            COL_TD_ACTIONS => true,
        ],
        "name"   => [
            COL_TITLE => "Nom complet",
        ],
        "active" => [
            COL_TITLE => "Active",
        ],
        "ageMin" => [
            COL_TITLE => "Age minimum",
        ],
        "ageMax" => [
            COL_TITLE => "Age maximum",
        ],
    ];
    if (!utils()->hasRootRole()) {
      unset($this->colDefinition["id"]);
    }
    dbUtil()->tables["configTab"] = [
        DB_COLS_DEFINITION => $this->colDefinition,
        DB_RIGHTS_ACCES    => TABLE_RIGHT_EDIT | TABLE_RIGHT_DELETE | TABLE_RIGHT_USER_ACTION
    ];
  }

  function __construct()
  {
    $this->buildTableDef();

    parent::__construct("configTab");
    $this->buildFullTable();
  }

  protected function getConfigObject(): ConfigBase
  {
    return ConfigProject::get(CONFIG_MEMBERS_CATEGORIES);
  }

  protected function getConfigRow()
  {
    return [
        "id"     => $_REQUEST["id"],
        "short"  => $_REQUEST["short"],
        "name"   => $_REQUEST["name"],
        "active" => $_REQUEST["val_active"],
        "ageMin" => $_REQUEST["ageMin"],
        "ageMax" => $_REQUEST["ageMax"],
        "code"   => $_REQUEST["code"],
    ];
  }
}
