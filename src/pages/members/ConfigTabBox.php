<?php

class ConfigTabBox
{

  public function __construct()
  {
    echo "<div id=membersBox class=container>";
    switch ($_REQUEST["active"]) {
      case "categories":
        new Categories();
        break;

      case "functions":
        new Functions();
        break;

      case "sizes":
        new Sizes();
        break;
    }
    echo "</div>";
    utils()->insertReadyFunction("configTabListener");
  }

}

function axConfigTabSubmit()
{
  utils()->axRefreshElement("members");
}

function axConfigTabEdit()
{
  switch ($_REQUEST["active"]) {
    case "categories":
      if (utils()->action == "delete") {
        $team = ($row = ConfigProject::get(CONFIG_MEMBERS_CATEGORIES)->getByIndex($_REQUEST["primary"][0]))["id"] * 10;
        if (mysqli_num_rows(dbUtil()->selectRow("membersSeason", "ri", "team >=$team and team<" . ($team + 10) . " limit 1", false))) {
          msgBox("<p>La catégorie «" . $row["name"] . "» est utilisée, elle ne peut être supprimée", "Annulé");
          return;
        }
        msgBox("<p>Confirmation suppression catégorie.</p>", "Confirmation", null, [MSGBOX_BUTTON_ACTION => "Supprimer", MSGBOX_BUTTON_CLOSE => "annuler"]);
        return;
      }
      new CategoriesEdit;
      return;

    case "functions":
      if (utils()->action == "delete") {
        $row = ConfigProject::get(CONFIG_MEMBERS_FUNCTIONS)->getByIndex($_REQUEST["primary"][0]);
        if (mysqli_num_rows(dbUtil()->selectRow("membersSeason", "ri", "functions like '%-" . $row["id"] . "-%' limit 1", false))) {
          msgBox("<p>La fonction «" . $row["name"] . "» est utilisée, elle ne peut être supprimée", "Annulé");
          return;
        }
        msgBox("<p>Confirmation suppression fonction.</p>", "Confirmation", null, [MSGBOX_BUTTON_ACTION => "Supprimer", MSGBOX_BUTTON_CLOSE => "annuler"]);
        return;
      }
      new FunctionsEdit();
      return;

    case "roles":
      new rolesEdit();
      return;

    case "sizes":
      if (utils()->action == "delete") {
        $row = ConfigProject::get(CONFIG_MEMBERS_SIZES)->getByIndex($_REQUEST["primary"][0]);
        if (mysqli_num_rows(dbUtil()->selectRow("members", "num", "sz=" . $row["id"] . " limit 1", false))) {
          msgBox("<p>La taille «" . $row["name"] . "» est utilisée, elle ne peut être supprimée", "Annulé");
          return;
        }
        msgBox("<p>Confirmation suppression Taille.</p>", "Confirmation", null, [MSGBOX_BUTTON_ACTION => "Supprimer", MSGBOX_BUTTON_CLOSE => "annuler"]);
        return;
      }
      new SizesEdit();
      return;
  }
}
