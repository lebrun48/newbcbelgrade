<?php

class Functions extends ConfigTable
{

  protected function buildTableDef()
  {
    $this->colDefinition = [
        "index"  => [
            COL_HIDDEN  => true,
            COL_PRIMARY => true,
        ],
        "id"     => [
            COL_TITLE => "id"
        ],
        "name"   => [
            COL_TITLE      => "Nom complet",
            COL_TD_ACTIONS => true,
        ],
        "role"   => [
            COL_TITLE => "Rôle (droits accès)",
        ],
        "system" => [
            COL_TITLE => "System",
        ],
    ];
    if (!utils()->hasRootRole()) {
      unset($this->colDefinition["id"]);
      unset($this->colDefinition["system"]);
    }
    dbUtil()->tables["configTab"] = [
        DB_COLS_DEFINITION => $this->colDefinition,
        DB_RIGHTS_ACCES    => TABLE_RIGHT_EDIT | TABLE_RIGHT_DELETE | TABLE_RIGHT_USER_ACTION
    ];
  }

  function __construct()
  {
    $this->buildTableDef();

    parent::__construct("configTab");
    $this->buildFullTable();
  }

  protected function getConfigObject(): ConfigBase
  {
    return ConfigProject::get(CONFIG_MEMBERS_FUNCTIONS);
  }

  protected function getConfigRow()
  {
    return [
        "id"     => $_REQUEST["id"],
        "name"   => $_REQUEST["name"],
        "role"   => $_REQUEST["role"],
        "system" => $_REQUEST["system"]
    ];
  }

  protected function getDisplayValue($key, $row)
  {
    switch ($key) {
      case "role":
        return ConfigProject::get()->getRoles()[$row[$key]];
    }
    return parent::getDisplayValue($key, $row);
  }

}
