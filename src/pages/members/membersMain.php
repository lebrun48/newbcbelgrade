<?php

class MembersMain
{

  private $active;
  private $paramShowed;
  private $params = ["categories" => "Catégories", "functions" => "Fonctions", "roles" => "Rôles", "sizes" => "Tailles"];

  function __construct()
  {
    !utils()->getPermanentValue("year", "members") &&
            ($_REQUEST["active"] = "admin") &&
            ($_REQUEST["year"] = ConfigFinances::get()->yearSeason) &&
            ($_REQUEST["withPaging"] = 1) &&
            $_REQUEST["withCells"] = 1;
    dbUtil()->tables["membersSeason"][DB_DEFAULT_WHERE] = "ms.season=" . $_REQUEST["year"];
    
    echo ""
    . "<main id=main>"
    . "  <form id=mainFormAction class='d-flex justify-content-between' style=align-items:center>"
    . "    <input form=mainFormAction type=hidden name=active value=" . $_REQUEST["active"] . ">"
    . "    <input form=mainFormAction type=hidden name=paramShowed value=" . $_REQUEST["paramShowed"] . ">"
    . "    <div class=container>"
    . "      <div class='d-flex' style=align-items:center>"
    . "        <div class=pb-2>"
    . "          <ul class='container pt-3 text-center nav nav-tabs'>"
    . "            <li class=' nav-item'>"
    . "              <a class='nav-link" . (($this->active = $_REQUEST["active"]) == "admin" ? " active" : '') . "' onclick=activateMembers('admin') data-toggle='tab'>Administratif</a>"
    . "            </li>"
    . "            <li class='nav-item'>"
    . "              <a class='nav-link" . ($this->active == "sport" ? " active" : '') . "' onclick=activateMembers('sport') data-toggle='tab'>Sportif</a>"
    . "            </li>"
    . "            <li class='nav-item'>"
    . "              <a class='nav-link" . ($this->active == "coti" ? " active" : '') . "' onclick=activateMembers('coti') data-toggle='tab'>Cotisation</a>"
    . "            </li>"
    . "            <li class='nav-item'" . ($this->params[$this->paramShowed = $_REQUEST["paramShowed"]] ? '>' : " style=display:none>")
    . "              <a class='nav-link" . ($this->params[$this->active] ? " active'>" : "'>")
    . "                <span onclick=activateMembers('$this->paramShowed',true)>" . $this->params[$this->paramShowed] . "</span><i class='fas fa-times icon-action ml-2 mr-0' onclick=activateMembers('admin',false)></i>"
    . "              </a>"
    . "            </li>"
    . "          </ul>"
    . "        </div>"
    . "        <div class='row" . ($_REQUEST["page"] == "configTab" ? " d-none" : '') . "'>"
    . "          <div class='col main-shown'>";
    $this->buildOption();
    echo ""
    . "          </div>"
    . "          <div class='col main-shown'>";
    $this->buildYear();
    echo ""
    . "          </div>"
    . "        </div>"
    . "      </div>"
    . "    </div>";
    $this->buildLocalMenu();
    echo ""
    . "  </form>"
    . "  <div class='tab-content pt-3'>"
    . "    <div class='tab-pane fade show active' id=mainTab>";
    new MembersBox();
    echo ""
    . "    </div>"
    . "    <div class='tab-pane fade' id=configTabBox>"
    . "    </div>"
    . "  </div>"
    . "  " . utils()->getPermanentNamesForm()
    . "</main>";
    utils()->insertReadyFunction("membersHeaderListener");
  }

  function buildLocalMenu()
  {
    echo ""
    . "<div class='btn-group dropleft' id=localMenuDef>"
    . "  <i class='fa-lg fas fa-cog' data-toggle='dropdown'></i>"
    . "  <div class='dropdown-menu dropdown-dark'>"
    . "    <a class='dropdown-item' onclick=activateMembers('categories',true)>Catégories équipes</a>"
    . "    <a class='dropdown-item' onclick=activateMembers('functions',true)>Fonctions des membres</a>"
    . "    <a class='dropdown-item' onclick=activateMembers('roles',true)>Rôles (droits d'accès)</a>"
    . "    <a class='dropdown-item' onclick=activateMembers('sizes',true)>Tailles vêtements</a>"
    . "  </div>"
    . "</div>";
  }

  function buildOption()
  {
    echo ""
    . "<div class='d-flex px-3 mt-2 ml-2 dropdown text-center'>"
    . "  <a class='px-0 nav-link dropdown-toggle' data-toggle='dropdown'>"
    . "    <img style=height:20px src='" . Plugin::getPluginRequestFilePath("finances/img/equalizer.png") . "'><span class='d-none d-md-inline ml-sm-2'>Options</span>"
    . "  </a>"
    . "  <div class='dropdown-menu dropdown-success'>"
    . "    <a class = 'dropdown-item options" . ($this->active != "admin" ? " d-none" : '') . "'>"
    . "      <input type=hidden name=withCells" . ($_REQUEST["withCells"] ? " value=1" : "") . "><i class='fas fa-check fa-lg mr-1' style=visibility=visible></i>Avec cellules"
    . "    </a>"
    . "    <a class='dropdown-item options'>"
    . "      <input type=hidden name=withPicture" . ($_REQUEST["withPicture"] ? " value=1" : "") . "><i class='fas fa-check fa-lg mr-1' style=visibility:hidden></i>Avec photos"
    . "    </a>"
    . "    <a class='dropdown-item options'>"
    . "      <input type=hidden name=withPaging" . ($_REQUEST["withPaging"] ? " value=1" : "") . "><i class='fas fa-check fa-lg mr-1' style=visibility:visible></i>Pagination"
    . "    </a>"
    . "  </div>"
    . "</div>";
  }

  function buildYear()
  {
    //check build membersSeason
    $res = dbUtil()->selectRow("membersSeason", "season", "1 group by season desc", false);
    echo ""
    . "<select style='width:130px;margin-top:10px' name='year' class='custom-select' onchange=pageAction('members')>";
    while ($tup = dbUtil()->fetch_row($res)) {
      echo "<option value=$tup[0]" . ($tup[0] == $_REQUEST["year"] ? " selected" : '') . ">$tup[0] - " . ($tup[0] + 1) . "</option>";
    }
    echo "</select>";
  }

}

function buildMain()
{
  new MembersMain();
}

function axMembersSubmit()
{
  if (utils()->action == "nopaging") {
    if ($_REQUEST["withPicture"]) {
      utils()->axExecuteJS("showToast", "La pagination ne peut pas être supprimée lorsque les photos sont visibles.");
      return;
    }
    unset($_REQUEST["withPaging"]);
    utils()->axRefreshMain();
    return;
  }
  if ($_REQUEST["withPicture"] && !$_REQUEST["withPaging"]) {
    $_REQUEST["withPaging"] = 1;
    utils()->axRefreshMain();
    utils()->axExecuteJS("showToast", "La pagination ne peut pas être supprimée lorsque les photos sont visibles.");
    return;
  }
  utils()->axRefreshMain();
}

function axMembersEdit()
{
  ConfigProject::get();
  switch (utils()->action) {
    case "insert":
    case "multiUpdate":
    case "update":
    case "addExisting":
    case "duplicate":
    case "cellUpdate":
      (new MembersEdit())->axBuildEditMsgBox();
      return;

    case "deleteOther":
      utils()->xAction = "submit";
      utils()->axRefreshElement("members");
      return;

    case "multiDelete":
      $keys = implode(",", utils()->flattenArray($_REQUEST["multiKeys"]));
      if ($n = dbUtil()->result(dbUtil()->selectRow("members", "count(*)", "num in ($keys) and affiliation=" . AFFILIATION_CLUB), 0)) {
        utils()->axExecuteJS("showToast", "$n membre" . ($n > 1 ? "s sont encore affiliés" : " est encore affilié") . " au club. Suppression annulée.");
        return;
      }
      $n = dbUtil()->result(dbUtil()->selectRow("members", "count(*)", "num in ($keys)"), 0);
      msgBox("<p><i class='fas fa-exclamation-triangle fa-2x red-text mr-2'></i> $n membre" . ($n > 1 ? "s seront supprimés!" : " sera supprimé") . "</p>"
              . "<p>Confirmation suppression multiple membres.</p> ", "Confirmation", MODAL_SIZE_SMALL, [MSGBOX_BUTTON_ACTION => "Supprimer", MSGBOX_BUTTON_CLOSE => "annuler"]);
      return;

    case "delete":
      if (mysqli_num_rows(dbUtil()->selectRow("members", "num", "num=" . $_REQUEST["primary"][0] . " and affiliation=" . AFFILIATION_CLUB . " limit 1"))) {
        utils()->axExecuteJS("showToast", "Un membre affilié au club ne peut pas être supprimé.");
        return;
      }
      msgBox("<p>Confirmation suppression membre.</p>", "Confirmation", MODAL_SIZE_SMALL, [MSGBOX_BUTTON_ACTION => "Supprimer", MSGBOX_BUTTON_CLOSE => "annuler"]);
      return;

    case "deleteGroup":
      if ($n = dbUtil()->result(dbUtil()->query("select count(*) from " . ($members = dbUtil()->getTableNameNoAs("members")) . " a left join $members b on b.cellRef=a.cellRef"
                      . " where a.num=" . $_REQUEST["primary"][0] . " and b.affiliation=" . AFFILIATION_CLUB), 0)) {
        utils()->axExecuteJS("showToast", "La cellule contient $n membre" . ($n > 1 ? "s affiliés" : " affilié") . " au club. Elle ne peut pas être supprimée.");
        return;
      }
      msgBox("<p><i class='fas fa-exclamation-triangle fa-2x red-text mr-2'></i>Tous les membres de la cellule seront supprimés!</p>"
              . "<p>Confirmation suppression cellule.</p> ", "Confirmation", MODAL_SIZE_SMALL, [MSGBOX_BUTTON_ACTION => "Supprimer", MSGBOX_BUTTON_CLOSE => "annuler"]);
      return;

    case "selectSearch":
      (new MembersEdit())->axBuildSelectSearch();
      return;

    case "getNewRow":
      (new MembersEdit())->axBuildNewFunctionRow();
      return;

    case "resetAddress":
      MembersEdit::axResetMembers();
      return;
  }
}
