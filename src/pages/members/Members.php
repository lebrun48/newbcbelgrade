<?php

class Members extends BuildTable
{

  private $curCell;
  private $otherCell;
  private $otherCellLine;
  public $withCell;

  //for functions
  const FUNCTIONS = 0;
  const MAIN_TEAM = 1;
  const TEAMS = 2;

  static function buildTableDef()
  {
    $def = [
        "num"           => [
            COL_PRIMARY     => true,
            COL_HIDDEN      => !utils()->hasRootRole(),
            COL_FILTER_TYPE => COL_FILTER_TEXT,
            COL_TD_ATTR     => "class=mr-2",
            COL_DB          => "mb.num",
            COL_TITLE       => "ID",
        ],
        "multiSel"      => [
            COL_TD_ATTR        => "class='d-none d-md-table-cell'",
            COL_TH_ATTR        => "class='d-none d-md-table-cell'",
            COL_SELECT_EDITION => true,
            COL_NO_DB          => true
        ],
        "picture"       => [
            COL_TITLE       => "Portait",
            COL_NO_DB       => true,
            COL_FILTER_TYPE => COL_FILTER_RADIO,
        ],
        "name"          => [
            COL_TITLE      => "Nom Prénom",
            COL_DB         => "concat(mb.name, ' ', ifnull(firstName,''))",
            COL_TD_ACTIONS => true,
            COL_ORDER      => true,
        ],
        "nameFilter"    => [
            COL_HIDDEN      => true,
            COL_FAST_SEARCH => true,
            COL_FILTER_TYPE => COL_FILTER_TEXT,
            COL_FILTER_SIZE => 6,
            COL_TITLE       => "Nom",
            COL_DB          => "mb.name"
        ],
        "affiliation"   => [
            COL_TITLE       => "Affiliation",
            COL_FILTER_TYPE => COL_FILTER_SELECT,
            COL_ORDER       => true,
            COL_TD_ATTR     => "class=text-nowrap",
            COL_FILTER_SIZE => 6,
        ],
        "sex"           => [
            COL_TITLE       => "Genre",
            COL_FILTER_TYPE => COL_FILTER_SELECT,
            COL_ORDER       => true,
            COL_FILTER_SIZE => 6,
            COL_ORDER       => true,
        ],
        "sz"            => [
            COL_TITLE       => "taille",
            COL_FILTER_TYPE => COL_FILTER_SELECT,
            COL_ORDER       => true,
            COL_TD_ATTR     => "class=text-nowrap",
            COL_FILTER_SIZE => 6,
            COL_ORDER_BY    => "sizeOrder"
        ],
        "functions"     => [
            COL_TITLE       => "Fonction",
            COL_EXT_REF     => ["membersSeason", "num", "ms.ri"],
            COL_DB          => "ms.functions",
            COL_FILTER_TYPE => COL_FILTER_SELECT,
            COL_ORDER_BY    => "functionOrder",
            COL_FILTER_SIZE => 6,
        ],
        "allTeams"      => [
            COL_FILTER_TYPE => COL_FILTER_SELECT,
            COL_TD_ATTR     => "class=text-nowrap",
            COL_TITLE       => "Equipes",
            COL_DB          => "ms.allTeams",
            COL_ORDER_BY    => "functionOrder asc, teamOrder",
            COL_FILTER_SIZE => 6,
        ],
        "remark"        => [
            COL_TITLE       => "Remarque",
            COL_FILTER_TYPE => COL_FILTER_TEXT,
            COL_TD_ATTR     => "style=max-width:10em class=text-truncate",
            COL_FILTER_SIZE => 4,
        ],
        "email"         => [
            COL_TITLE       => "EMail",
            COL_FILTER_TYPE => COL_FILTER_TEXT,
            COL_FILTER_SIZE => 4,
        ],
        "phone"         => [
            COL_TITLE => "GSM",
        ],
        "birth"         => [
            COL_TITLE   => "Age",
            COL_ORDER   => true,
            COL_TD_ATTR => "class=text-nowrap",
        ],
        "dateIn"        => [
            COL_TITLE => "Entrée",
            COL_TYPE  => "date[d/m/Y]",
            COL_ORDER => true,
        ],
        "function"      => [
            COL_HIDDEN => true,
            COL_DB     => "ms.function",
        ],
        "team"          => [
            COL_HIDDEN => true,
            COL_DB     => "ms.team",
        ],
        "streetId"      => [
            COL_HIDDEN  => true,
            COL_EXT_REF => ["cells", "cellRef", "ri"],
            COL_DB      => "if(cellRef, concat(cell.buildingNb,if(cell.box, concat(' Bte ', cell.box),'')),null)"
        ],
        "address"       => [
            COL_HIDDEN  => true,
            COL_EXT_REF => ["streets", "cell.streetID", "id"],
            COL_DB      => "street.name"
        ],
        "city"          => [
            COL_HIDDEN  => true,
            COL_EXT_REF => ["cities", "street.city", "ri"],
            COL_DB      => "city.name"
        ],
        "cellRef"       => [
            COL_GROUP        => 0,
            COL_GROUP_ACTION => "cellUpdate",
            COL_ORDER_BY     => "street.name,cell.buildingNb,cell.box",
            COL_TD_ATTR      => "colspan=15 class=group-col",
            COL_TD_ACTIONS   => true,
            COL_FILTER_TYPE  => COL_FILTER_TEXT,
            COL_FILTER_LABEL => "Adresse",
            COL_FILTER_SIZE  => 4,
        ],
        "otherCell"     => [
            COL_HIDDEN => true,
            COL_DB     => "cell.others"
        ],
        "teamOrder"     => [
            COL_HIDDEN  => true,
            COL_EXT_REF => ["orders o1", "ms.team div 10 and o1.keyIdx=" . CONFIG_MEMBERS_CATEGORIES, "id"],
            COL_DB      => "(o1.orderId*10+mod(ms.team, 10))"
        ],
        "functionOrder" => [
            COL_HIDDEN  => true,
            COL_EXT_REF => ["orders o2", "ms.function and o2.keyIdx=" . CONFIG_MEMBERS_FUNCTIONS, "id"],
            COL_DB      => "o2.orderId"
        ],
        "sizeOrder"     => [
            COL_HIDDEN  => true,
            COL_EXT_REF => ["orders o3", "mb.sz and o3.keyIdx=" . CONFIG_MEMBERS_SIZES, "id"],
            COL_DB      => "o3.orderId"
        ]
    ];

    if (!$_REQUEST["withPicture"]) {
      $def["picture"][COL_HIDDEN] = true;
    }

    dbUtil()->tables["members"][DB_COLS_DEFINITION] = $def;
  }

  function __construct($tableId = "members")
  {

//    utils()->getArrayFromDump('');
//    $_REQUEST["f_fastSearch"] = "beca";
//    debugLog($_REQUEST);
//    dbUtil()->seeRequest = 1;

    self::buildTableDef();
    parent::__construct($tableId);
  }

  protected function setCurrentEditedRow()
  {
    if (!$fromKeys = dbUtil()->getFromKeys()) {
      return;
    }
//    unset($this->colDefinition["teamOrder"]);
//    unset($this->colDefinition["functionOrder"]);
    unset($this->colDefinition["name"][COL_DB]);
    unset($this->colDefinition["addressCity"]);
    $this->colDefinition["firstName"] = [];
    $this->colDefinition["box"] = [COL_DB => "cell.box"];
    $this->colDefinition["buildingNb"] = [COL_DB => "cell.buildingNb"];
    $this->colDefinition["streetId"][COL_DB] = "cell.streetID";
    $this->colDefinition["city"][COL_DB] = "city.ri";
    $this->colDefinition["mainCity"] = [COL_DB => "street.mainCity"];
    $this->colDefinition["cellRefParent"] = [COL_DB => "mb.cellRefParent"];

    $this->row = dbUtil()->fetch_assoc(dbUtil()->query($this->getdBRequestAttr() . " "
                    . $this->getDbFrom()
                    . dbUtil()->getKeys(null, $this->getDbGlobalDefaultWhere() . $fromKeys, true, false)));
    !$this->row["city"] && $this->row["box"] = null;
    !$this->row["buildingNb"] && $this->row["buildingNb"] = null;
    $this->row["otherCell"] = $this->row["otherCell"] ? json_decode($this->row["otherCell"]) : [];

    $this->colDefinition = dbUtil()->getTable($this->tableId)[DB_COLS_DEFINITION];
  }

  protected function DBupdate()
  {
    $_REQUEST["val_birth"] && $_REQUEST["val_birth"] = DateTime::createFromFormat("d/m/Y", $_REQUEST["val_birth"])->format("Y-m-d");
    switch (utils()->action) {
      case "addExisting":
        dbUtil()->begin_transaction();
        $this->row["num"] = $_REQUEST["num"];
        $this->updateCellOther($_REQUEST["cellRefParent"], false);
        dbUtil()->commit();
        return;

      case "cellUpdate":
        dbUtil()->begin_transaction();
        $cellRi = $_REQUEST["cellRef"];
        try {
          !$_REQUEST["buildingNb"] && $_REQUEST["buildingNb"] = "0";
          !$_REQUEST["box"] && $_REQUEST["box"] = "=''";
          $_REQUEST["city"] && $result = dbUtil()->updateRow("cells", [
              "streetId"   => $_REQUEST["address"],
              "buildingNb" => $_REQUEST["buildingNb"],
              "box"        => $_REQUEST["box"]
                  ], "ri=$cellRi", true);
        } catch (Exception $exc) {
          if ($exc->getCode() == E_DB_DUPLICATE) {
            $newCell = dbUtil()->fetch_row(dbUtil()->selectRow("cells", "ri, others", ""
                            . "streetID=" . dbUtil()->real_escape_string($_REQUEST["address"]) . " and "
                            . "buildingNb='" . dbUtil()->real_escape_string(dbUtil()->extractValue($_REQUEST["buildingNb"])) . "' and "
                            . "box='" . dbUtil()->real_escape_string(dbUtil()->extractValue($_REQUEST["box"])) . "'"));
            dbUtil()->updateRow("members", "cellRef=$newCell[0]", "cellRef=" . $_REQUEST["cellRef"]);
            dbUtil()->updateRow("members", "cellRefParent=$newCell[0]", "cellRefParent=$cellRi");
            //merge cellRefOther
            $others = dbUtil()->result(dbUtil()->selectRow("cells", "others", "ri=$cellRi"), 0);
            $others = array_merge(json_decode($others) ?? [], json_decode($newCell[1]) ?? []);
            $others && dbUtil()->updateRow("cells", "others='" . json_encode($others) . "'", "ri=$newCell[0]");
            dbUtil()->deleteRow("cells", "ri=$cellRi", true, DB_OPTION_NO_THROW);
          }
          else
            throw $exc;
        }
        dbUtil()->commit();
        return;

      case "multiUpdate":
        utils()->action = "update";
        parent::DBupdate();
        return;

      case "deleteOther":
        dbUtil()->begin_transaction();
        $this->row = dbUtil()->fetch_assoc(dbUtil()->selectRow("members", "num,cellRefParent", "num=" . $_REQUEST["primary"][0]));
        $this->updateCellOther($this->row["cellRefParent"], true);
        dbUtil()->commit();
        return;

      case "deleteGroup":
        dbUtil()->begin_transaction();
        //search members in other cells
        $members = strtok($this->dbTableName, ' ');
        $res = dbUtil()->query("SELECT b.num,b.cellRefParent,b.cellRef FROM $members a left join $members b on b.cellRef=a.cellRef where a.num=" . $_REQUEST["primary"][0]);
        while ($this->row = dbUtil()->fetch_assoc($res)) {
          $cellRef = $this->row["cellRef"];
          $this->row["cellRefParent"] && $this->updateCellOther($this->row["cellRefParent"], true);
        }
        ($others = dbUtil()->result(dbUtil()->selectRow("cells", "others", "ri=$cellRef"), 0)) && ($others = json_decode($others)) || $others = [];
        foreach ($others as $other) {
          dbUtil()->updateRow("members", "cellRefParent=null", "num=$other");
        }
        dbUtil()->deleteRow("members", "cellRef=$cellRef");
        dbUtil()->deleteRow("cells", "ri=$cellRef", true, DB_OPTION_NO_THROW);
        dbUtil()->commit();
        return;

      case "update":
        //update member
        dbUtil()->begin_transaction();
        $this->updateCells();
        parent::DBupdate();
        //remove currentCell if no others
        $this->row["cellRef"] && dbUtil()->deleteRow("cells", "ri=" . $this->row["cellRef"], true, DB_OPTION_NO_THROW);
        $this->updateMembersSeason();
        dbUtil()->commit();
        return;

      case "multiDelete":
      case "delete":
        dbUtil()->begin_transaction();
        $res = dbUtil()->selectRow("members", "num, cellRefParent, cellRef", dbUtil()->getFromKeys());
        while ($this->row = dbUtil()->fetch_assoc($res)) {
          ($p = $this->row["cellRefParent"]) && $this->updateCellOther($p, true);
          dbUtil()->deleteRow("members", "num=" . $this->row["num"]);
          ($cell = $this->row["cellRef"]) && dbUtil()->deleteRow("cells", "ri=$cell", true, DB_OPTION_NO_THROW);
        }
        dbUtil()->commit();
        return;

      case "addNew":
      case "duplicate":
        utils()->action = "insert";
      case "insert":
        dbUtil()->begin_transaction();
        $this->insertRow();
        dbUtil()->commit();
        return;
    }
  }

  private function insertRow()
  {
    if ($this->updateCells()) {
      msgBox("<p>La cellule existe déjà!</p><p>Utiliser la fonction 'Ajouter un nouveau membre' (icône <i class='fas fa-plus-circle'></i>) sur la ligne de la cellule."
              . "</p><p>L'ajout du membre est annulé.</p>", "Cellule existe");
      return;
    }
    //insert in members
    parent::DBupdate();
    dbUtil()->seeRequest = 1;
    $ri = dbUtil()->getDbCnx()->insert_id;
    //insert in membersSeason
    if ($functions = $this->getFunctions()) {
      //update roles
      dbUtil()->updateRow("members", ["roles" => implode('-', ConfigProject::get()->getUserRoles($functions[self::FUNCTIONS]))], "num=$ri");
      // insert current season
      dbUtil()->insertRow("membersSeason", [
          "team"      => $functions[self::MAIN_TEAM],
          "allTeams"  => $functions[self::TEAMS] ? json_encode($functions[self::TEAMS]) : null,
          "function"  => $functions[self::FUNCTIONS][0],
          "functions" => '-' . implode('-', $functions[self::FUNCTIONS]) . '-',
          "ri"        => $ri
      ]);
      //insert next season if existing
      dbUtil()->getTable("membersSeason")[DB_DEFAULT_INSERT] = null;
      $curYear = ConfigFinances::get()->yearSeason;
      ConfigProject::get()->getYearStatus() == CONFIG_YEAR_STATUS_NEW_YEAR && ($nextYear = $curYear + 1) &&
              dbUtil()->insertRow("membersSeason", [
                  "team"      => $functions[self::MAIN_TEAM],
                  "allTeams"  => $functions[self::TEAMS] ? json_encode($functions[self::TEAMS]) : null,
                  "function"  => $functions[self::FUNCTIONS][0],
                  "functions" => '-' . implode('-', $functions[self::FUNCTIONS]) . '-',
                  "season"    => $nextYear,
                  "ri"        => $ri
                      ],);
    }
    if (!$_REQUEST["val_cellRef"]) {
      dbUtil()->insertRow("cells", ["streetId" => -2, "buildingNb" => $ri]);
      dbUtil()->updateRow("members", "cellRef=" . ($_REQUEST["val_cellRef"] = dbUtil()->getDbCnx()->insert_id), "num=$ri");
    }
    if (ConfigProject::get()->hasRole($functions[self::FUNCTIONS], ROLE_PLAYER)) {
      $_REQUEST["email1"] && dbUtil()->insertRow("members", [
                  "name"      => $_REQUEST["name1"],
                  "firstName" => $_REQUEST["firstName1"],
                  "email"     => $_REQUEST["email1"],
                  "phone"     => $_REQUEST["phone1"],
                  "cellRef"   => $_REQUEST["val_cellRef"],
                  "roles"     => implode('-', ConfigProject::get()->getUserRoles($functions[self::FUNCTIONS]))
              ]) && dbUtil()->insertRow("membersSeason", [
                  "function"  => FUNCTION_PARENT,
                  "functions" => "-" . FUNCTION_PARENT . "-",
                  "season"    => $curYear,
                  "ri"        => $ri = dbUtil()->getDbCnx()->insert_id
              ]) && $nextYear && dbUtil()->insertRow("membersSeason", [
                  "function"  => FUNCTION_PARENT,
                  "functions" => "-" . FUNCTION_PARENT . "-",
                  "season"    => $nexYear,
                  "ri"        => $ri = dbUtil()->getDbCnx()->insert_id
      ]);

      $_REQUEST["email2"] && dbUtil()->insertRow("members", [
                  "name"      => $_REQUEST["name2"],
                  "firstName" => $_REQUEST["firstName2"],
                  "email"     => $_REQUEST["email12"],
                  "phone"     => $_REQUEST["phone12"],
                  "cellRef"   => $_REQUEST["val_cellRef"],
                  "roles"     => implode('-', ConfigProject::get()->getUserRoles($functions[self::FUNCTIONS]))
              ]) && dbUtil()->insertRow("membersSeason", [
                  "function"  => FUNCTION_PARENT,
                  "functions" => "-" . FUNCTION_PARENT . "-",
                  "ri"        => $ri = dbUtil()->getDbCnx()->insert_id
              ]) && $nextYear && dbUtil()->insertRow("membersSeason", [
                  "function"  => FUNCTION_PARENT,
                  "functions" => "-" . FUNCTION_PARENT . "-",
                  "season"    => $nexYear,
                  "ri"        => $ri = dbUtil()->getDbCnx()->insert_id
      ]);
    }
    !$_REQUEST["f_fastSearch"] && $_REQUEST["f_fastSearch"] = $_REQUEST["val_name"] . ' ' . $_REQUEST["val_firstName"];
  }

  private function updateCellOther($parent, $toRemove)
  {
    ($others = dbUtil()->result(dbUtil()->selectRow("cells", "others", "ri=$parent"), 0)) && ($others = json_decode($others)) || $others = [];
    if ($toRemove) {
      unset($others[array_search($this->row["num"], $others)]);
      $others = array_values($others);
    }
    else {
      array_search($this->row["num"], $others) === false && $others[] = $this->row["num"];
    }
    dbUtil()->updateRow("cells", ["others" => $others ? json_encode($others) : "=null"], "ri=$parent");
    dbUtil()->updateRow("members", "cellRefParent=" . ($toRemove ? "null" : $parent), "num=" . $this->row["num"]);
  }

  protected function getActionName($action)
  {
    return $action == "delete" && $this->otherCellLine ? "deleteOther" : $action;
  }

  protected function updateCells()
  {
    $this->setCurrentEditedRow();
    if ($_REQUEST["cellVisible"] && $this->row &&
            ($this->row["city"] != $_REQUEST["city"] ||
            $this->row["streetId"] != $_REQUEST["address"] ||
            $this->row["buildingNb"] != $_REQUEST["buildingNb"] ||
            $this->row["box"] != $_REQUEST["box"])) {
      //new CellRef
      !$_REQUEST["buildingNb"] && $_REQUEST["buildingNb"] = "0";
      !$_REQUEST["box"] && $_REQUEST["box"] = "=''";
      ($newCell = dbUtil()->result(dbUtil()->selectRow("cells", "ri", ""
                      . "streetID=" . dbUtil()->real_escape_string(dbUtil()->extractValue($_REQUEST["address"])) . " and "
                      . "buildingNb='" . dbUtil()->real_escape_string(dbUtil()->extractValue($_REQUEST["buildingNb"])) . "' and "
                      . "box='" . dbUtil()->real_escape_string(dbUtil()->extractValue($_REQUEST["box"])) . "'"), 0)) && $existing = true;
      ;
      !$newCell && dbUtil()->insertRow("cells", [
                  "streetID"   => $_REQUEST["address"],
                  "buildingNb" => $_REQUEST["buildingNb"],
                  "box"        => $_REQUEST["box"]
              ]) && $newCell = dbUtil()->getDbCnx()->insert_id;
      $_REQUEST["val_cellRef"] = $newCell;

      //update cellRefParent && others in parent cells
      if (ConfigProject::get()->hasRole($this->row["functions"], ROLE_PARENT) &&
              (!($parentCell = $this->row["cellRefParent"]) && ($parentCell = $_REQUEST["val_cellRefParent"] = $this->row["cellRef"]) || $isSame = $parentCell == $newCell)) {
        ($others = dbUtil()->result(dbUtil()->selectRow("cells", "others", "ri=$parentCell"), $row)) && $others = json_decode($others);
        if ($isSame) {
          //remove form others
          $_REQUEST["val_cellRefParent"] = "";
          unset($others[array_search($this->row["num"], $others)]);
        }
        else {
          $others[] = $this->row["num"];
        }
        dbUtil()->updateRow("cells", ["others" => $others ? json_encode($others) : "=null"], "ri=$parentCell");
      }
    }
    return $existing;
  }

  private function updateMembersSeason()
  {
    ($functions = $this->getFunctions()
            ) && dbUtil()->updateRow("membersSeason", [
                "team"      => $functions[self::MAIN_TEAM],
                "allTeams"  => $functions[self::TEAMS] ? json_encode($functions[self::TEAMS]) : null,
                "function"  => $functions[self::FUNCTIONS][0],
                "functions" => '-' . implode('-', $functions[self::FUNCTIONS]) . '-'
                    ], "ri=" . $_REQUEST["primary"][0]
            ) && dbUtil()->updateRow("members", [
                "roles" => implode('-', ConfigProject::get()->getUserRoles($functions[self::FUNCTIONS]))
                    ], "ri=" . $_REQUEST["primary"][0]);
  }

  private function getFunctions()
  {
    foreach ($_REQUEST as $key => $val) {
      $type = substr($key, 0, 4);
      ($type == "func" || $type == "team" || $type == "tSet") && $function[substr($key, 4)][$type] = $val;
    }

    if ($function) {
      // set teams for player and coach
      foreach ($function as $idx => $ar) {
        if (ConfigProject::get()->hasRole($ar["func"], [ROLE_COACH, ROLE_PLAYER])) {
          $ar["team"] && ($team[$ar["func"]][] = '-' . ($t = $ar["team"] * 10 + $ar["tSet"]) . '-') && !$mainTeam && $mainTeam = $t;
        }
      }
      // set functions
      $functions = [self::FUNCTIONS => array_unique(array_column($function, "func")), self::MAIN_TEAM => $mainTeam, self::TEAMS => $team];

      sort($functions[self::FUNCTIONS]);
      return $functions;
    }
  }

  protected function buildSql()
  {
    if (!$this->withCell) {
      return parent::buildSql();
    }
    $where = $this->getDbWhere() . " group by cellRef";
    $list = dbUtil()->fetch_all(dbUtil()->query("select cellRef " . $this->getDbFrom() . dbUtil()->getKeys($this->tableId, $where, true, false)));
    $list = !$list ? [0] : array_column($list, 0);
    return $this->getdBRequestAttr() . " "
            . $this->getDbFrom()
            . dbUtil()->getKeys($this->tableId, $this->getDbGlobalDefaultWhere() . dbUtil()->getSQLSelectAttrIn("cellRef", $list), true, false)
            . $this->getDbOrder()
            . $this->getDbLimit();
  }

  protected function buildLastLine()
  {
    $this->curCell = null;
    if (!$this->otherCell) {
      return;
    }
    $this->otherCellLine = true;
    $this->otherCell = json_decode($this->otherCell);
    $res = dbUtil()->query($this->getdBRequestAttr() . " "
            . $this->getDbFrom()
            . dbUtil()->getKeys($this->tableId, $this->getDbGlobalDefaultWhere() . "mb.num in (" . implode(",", $this->otherCell) . ")", true, false));
    while ($tup = dbUtil()->fetch_assoc($res)) {
      $tup["cellRef"] = -1;
      parent::buildLine($tup);
    }
    $this->otherCell = $this->otherCellLine = null;
  }

  protected function buildLine($row)
  {
    if (!$this->withCell) {
      return parent::buildLine($row);
    }
    !$this->curCell && $this->curCell = $row["cellRef"];
    if ($this->curCell == $row["cellRef"]) {
      $this->otherCell = $row["otherCell"];
      return parent::buildLine($row);
    }
    //display other cell when cellRef change
    $this->buildLastLine();
    $this->buildLine($row);
  }

  protected function getUserAction($row)
  {
    if ($this->isGroupLine) {
      if (mb_substr($row["address"], 0, 1, "UTF-8") != '«') {
        return "<i title='" . htmlspecialchars("Ajouter un membre d'une autre cellule (parent séparé)", ENT_QUOTES)
                . "' class='fas fa-user-plus fa-sm icon-action' data-action='addExisting'></i>";
      }
    }
    elseif ($this->row["name"] && $row["cellRef"] != -1) {
      return "<i title='Dupliquer membre dans la cellule' class='far fa-copy fa-lg icon-action' data-action='duplicate'></i>";
    }
  }

  protected function getTrAttributes($row = null)
  {
    return $row && $row["cellRef"] == -1 ? "class='font-italic grey-text'" : parent::getTrAttributes($row);
  }

  protected function hasGroupValueChanged($row, $key, $currentValue): bool
  {
    return $row["cellRef"] == -1 ? false : parent::hasGroupValueChanged($row, $key, $currentValue);
  }

  protected function getFilterModalAttr()
  {
    return [MODAL_SCROLLABLE => true];
  }

  protected function getFilterDisplayValue($key, $val)
  {
    switch ($key) {
      case "allTeams":
        return ConfigProject::get(CONFIG_MEMBERS_CATEGORIES)->getPropById($val, "short");
    }
    return parent::getFilterDisplayValue($key, $val);
  }

  protected function getFilterOptionsArrayValues($key)
  {
    switch ($key) {
      case "sex":
        $quote = "&#039;";
        return [[null => "(Non assigné)", "{$quote}M{$quote}" => "Masculin", "{$quote}F{$quote}" => "Féminin"]];

      case "functions":
        return [ConfigProject::get(CONFIG_MEMBERS_FUNCTIONS)->getNameListById(true)];

      case "affiliation":
        return [ConfigProject::get()->affiliationType];

      case "allTeams":
        return [ConfigProject::get(CONFIG_MEMBERS_CATEGORIES)->getUsedTeamsName()];
    }
    return parent::getFilterOptionsArrayValues($key);
  }

  protected function getFiltersDataOperationSQL($key, $operation, $filterValue)
  {
    if ($key == "cellRef") {
      return "(" . parent::getFiltersDataOperationSQL("address", $operation, $filterValue) . " or " . parent::getFiltersDataOperationSQL("city", $operation, $filterValue) . ")";
    }
    return parent::getFiltersDataOperationSQL($key, $operation, $filterValue);
  }

  protected function getFiltersDataTranslatedValue($key, $filterValue, $isStrict = false)
  {
    switch ($key) {
      case "name":
        if (is_numeric($filterValue)) {
          return;
        }
        return $filterValue;

      case "num":
        if (!is_numeric($filterValue)) {
          return;
        }
        return $filterValue;

      case "sex":
        if (is_string($filterValue) && (strlen($filterValue) > 1 || $filterValue != 'M' || $filterValue != 'F')) {
          return;
        }
        return $filterValue;

      case "functions":
        if (is_string($filterValue)) {
          if (!($ar = utils()->filterArray(ConfigProject::get(CONFIG_MEMBERS_FUNCTIONS)->getNameListById(), $filterValue, $isStrict))) {
            return;
          }
        }
        !$ar && ($ar = $filterValue);
        foreach ($ar as $val) {
          $filter .= "|" . ($isStrict ? '=' : '') . "-$val-";
        }
        return substr($filter, 1);

      case "affiliation":
        if (is_string($filterValue)) {
          if (!($ar = utils()->filterArray(ConfigProject::get()->affiliationType, $filterValue, $isStrict))) {
            return;
          }
        }
        return $ar ?? $filterValue;

      case "allTeams":
        if (is_string($filterValue)) {
          if (!($ar = utils()->filterArray(ConfigProject::get(CONFIG_MEMBERS_CATEGORIES)->getUsedTeamsName(), $filterValue, $isStrict))) {
            return;
          }
        }
        !$ar && $ar = $filterValue;
        foreach ($ar as $val) {
          $filter .= "|" . ($isStrict ? "[\"-$val-\"]" : "-$val-");
        }
        return substr($filter, 1);

      case "sz":
        if (is_string($filterValue)) {
          if (!($ar = utils()->filterArray(ConfigProject::get(CONFIG_MEMBERS_SIZES)->getNameListById(), $filterValue, $isStrict))) {
            return;
          }
        }
        return $ar ?? $filterValue;

      case "phone":
        $token = strtok($filterValue, " ./");
        while ($token) {
          if (!is_numeric($token)) {
            return;
          }
          $token = strtok(" ./");
        }
        return $filterValue;

      case "birth":
        return;

      case "dateIn":
        $tab = explode("/", $filterValue);
        if (sizeof($tab) > 3) {
          return;
        }
        foreach ($tab as $val) {
          if (!empty($val) && !is_numeric($val)) {
            return;
          }
        }
        return ($isStrict ? "=" : '') . implode('-', array_reverse($tab));
    }

    return parent::getFiltersDataTranslatedValue($key, $filterValue, $isStrict);
  }

  protected function getLimit()
  {
    return $_REQUEST["withPaging"] ? 30 : null;
  }

  protected function getRightsOnLine($row, $currentRights)
  {
    return $this->otherCellLine ? $currentRights &= ~TABLE_RIGHT_EDIT & ~TABLE_RIGHT_EDIT_MULTIPLE : parent::getRightsOnLine($row, $currentRights);
  }

  protected function getTdAttributes($key, $row, $attr)
  {
    switch ($key) {
      case "birth":
        if ($row[$key]) {
          return addAttribute($attr, DateTime::createFromFormat("Y-m-d", $row[$key])->format("d/m/Y"), "title");
        }
        break;

      case "remark":
        return addAttribute($attr, $row[$key], "title");

      case "email":
        if (!$row["email"] &&
                ConfigProject::get()->hasRole($row["functions"], ROLE_PLAYER) &&
                $row["birth"] && utils()->now()->diff(new DateTime($row["birth"]), true)->y >= 18) {
          return addAttribute(addAttribute($attr, "manquant!", "title"), "orange lighten-5");
        }
    }
    return parent::getTdAttributes($key, $row, $attr);
  }

  protected function getDisplayValue($key, $row)
  {
    switch ($key) {
      case "picture":

        return "<img style=height:50px src='"
                . (utils()->fileExists($f = ConfigProject::get()->getPicturePathMember($row["num"])) ? $f : utils()->imgPath
                . ($row["sex"] == 'F' ? "/Age-Child-Female-Light-icon.png" : "/Person-Male-Light-icon.png")) . "'>";

      case "affiliation":
        return ConfigProject::get()->affiliationType[$row[$key] ?? 0];

      case "cellRef" :
        return $row["address"] . (mb_substr($row["address"], 0, 1, "UTF-8") == '«' || $row["streetId"] == '0' ? '' : " " . $row["streetId"])
                . " " . $row["city"] . (utils()->hasRootRole() ? "  <span style=font-size:medium>(" . $row[$key] . ")</span>" : '');

      case "sz":
        return ConfigProject::get(CONFIG_MEMBERS_SIZES)->getNameById($row[$key]);

      case "functions":
        $functions = explode("-", substr($row[$key], 1, -1));
        return ConfigProject::get()->getFunctionsName($functions);

      case "allTeams":
        $teams = json_decode(str_replace('-', '', $row[$key]), true);
        if (!$teams) {
          return;
        }
        foreach ($teams as $function => $team) {
          $disp .= "<b>" . ConfigProject::get(CONFIG_MEMBERS_FUNCTIONS)->getNameById($function) . ": </b>";
          $disp .= ConfigProject::get()->getTeamShortName($team) . "<br>";
        }
        return substr($disp, 0, -4);

      case "birth":
        return ($dt = $row[$key]) ? utils()->now()->diff(new DateTime($dt), true)->y . " ans" : null;

      case "phone":
        if (utils()->browserName == "chrome") {
          return $row[$key] ? "<a class=td-no-action style=color:blue;text-decoration:underline;text-decoration-color:blue href='tel:$row[$key]'><i class='fas fa-phone mr-1'></i>$row[$key]</a>" : '';
        }
        return $row[$key];
    }
    return parent::getDisplayValue($key, $row);
  }

}
