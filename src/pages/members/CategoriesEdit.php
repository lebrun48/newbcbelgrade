<?php

class CategoriesEdit
{

  public function __construct()
  {
    utils()->action == "update" && $row = ConfigProject::get(CONFIG_MEMBERS_CATEGORIES)->getByIndex($_REQUEST["primary"][0]);

    $fields = [
        "!short"  => [
            ED_LABEL       => "Nom abrégé",
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_VALIDATE    => [
                ED_VALIDATE_REQUIRED   => true,
                ED_VALIDATE_INVALIDE   => "Manquant",
                ED_VALIDATE_MAX_LENGTH => 30,
            ],
            ED_FIELD_WIDTH => 4
        ],
        "!name"   => [
            ED_LABEL       => "Nom complet",
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_VALIDATE    => [
                ED_VALIDATE_REQUIRED   => true,
                ED_VALIDATE_MAX_LENGTH => 50,
                ED_VALIDATE_INVALIDE   => "Manquant"
            ],
            ED_FIELD_WIDTH => 8
        ],
        "!ageMin" => [
            ED_LABEL       => "Age minimum",
            ED_TYPE        => ED_TYPE_INTEGER,
            ED_VALIDATE    => [
                ED_VALIDATE_MIN_VALUE => 3,
                ED_VALIDATE_MAX_VALUE => 100,
                ED_VALIDATE_INVALIDE  => "Age compris entre 3 et 100 ans"
            ],
            ED_FIELD_WIDTH => 4
        ],
        "!ageMax" => [
            ED_LABEL       => "Age maximum",
            ED_TYPE        => ED_TYPE_INTEGER,
            ED_VALIDATE    => [
                ED_VALIDATE_MIN_VALUE => 3,
                ED_VALIDATE_MAX_VALUE => 100,
                ED_VALIDATE_INVALIDE  => "Age compris entre 3 et 100 ans"
            ],
            ED_FIELD_WIDTH => 4
        ],
        "active"  => [
            ED_LABEL       => "Active",
            ED_TYPE        => ED_TYPE_CHECK,
            ED_FIELD_WIDTH => 4
        ],
        "!code"   => [ED_TYPE => ED_TYPE_HIDDEN],
        "!id"     => [ED_TYPE => ED_TYPE_HIDDEN]
    ];

    msgBox(BuildForm::getForm($fields, $row), "Edition catégorie", null, [MSGBOX_BUTTON_ACTION => "Enregistrer", MSGBOX_BUTTON_CLOSE => "Annuler"]);
  }

}

/*{"1":{"id":"1","short":"SEN_N1_H","name":"S\u00e9niors 1er Nationale Hommes","disabled":"1","ageMin":"16","ageMax":null,"code":"1"},
 */