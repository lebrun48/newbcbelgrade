<?php

class MembersEdit extends Members
{

  private $fields = [];
  private $selectSearch;
  private $isInsert;
  private $addNew;
  private $buildCell;

  function __construct()
  {
    parent::__construct("members");
  }

  function axBuildEditMsgBox()
  {
    switch (utils()->action) {
      case "insert":
        $this->isInsert = true;
        $this->buildCell = true;
        $this->buildUpdate_InsertFields();
        $title = "Ajout membre";
        $size = MODAL_SIZE_LG;
        break;

      case "addExisting":
        $this->buildAddExistingFields();
        $title = "Ajout membre existant";
        break;

      case "multiUpdate":
        $title = "Modification multiple";
        $this->fields += [
            ED_MULTIPLE_ROW_EDIT => true,
            "affiliation"        => [
                ED_LABEL                  => "Affiliation",
                ED_TYPE                   => ED_TYPE_SELECT,
                ED_FIELD_WIDTH            => 6,
                ED_OPTIONS                => mdbCompos()->getOptions(ConfigProject::get()->affiliationType, "class=pb-0"),
                ED_SELECT_VISIBLE_OPTIONS => 8,
        ]];
        break;

      case "cellUpdate":
        $this->setCurrentEditedRow();
        $this->fields += ["!cellRef" => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => $this->row["cellRef"]]];
        $this->buildCellFields();
        $title = "Modification cellule";
        break;

      case "update":
        $title = "Edition membre";
      case "duplicate":
        $this->setCurrentEditedRow();
        $this->buildCell = $this->row["streetId"] >= 0;
        $this->row["functions"] = explode('-', substr($this->row["functions"], 1, -1));
        $this->row["allTeams"] = json_decode(str_replace("-", '', $this->row["allTeams"]), true);
        $this->row["birth"] && $this->row["birth"] = DateTime::createFromFormat("Y-m-d", $this->row["birth"])->format("d/m/Y");
        $size = MODAL_SIZE_LG;
        if (!$title) {
          //duplicate
          $title = "Duplication membre";
          $this->row["firstName"] = $this->row["birth"] = $this->row["sex"] = $this->row["email"] = $this->row["phone"] = $this->row["sz"] = '';
          $this->fields += ["cellRef" => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => $this->row["cellRef"]]];
          $this->buildCell = false;
        }
        $this->buildUpdate_InsertFields();
        break;
    }
    msgBox(BuildForm::getForm($this->fields, $this->row, "membersForm "), $title, $size, [
        MSGBOX_BUTTON_ACTION    => "Enregistrer",
        MSGBOX_BUTTON_CLOSE     => "Annuler",
        MSGBOX_MODAL_ATTR       => [MODAL_NO_ESC_KEY => true],
        MSGBOX_EXECUTE_ON_SHOWN => $this->selectSearch ? ["searchSelectListener", ["pageId" => "members", "selectIds" => $this->selectSearch]] : null,
    ]);
  }

  private function buildAddExistingFields()
  {
    $this->fields += [
        "!num"           => [
            ED_LABEL             => "Nom",
            ED_TYPE              => ED_TYPE_SELECT,
            ED_PLACEHOLDER       => '',
            ED_OPTIONS           => [[ED_LABEL => "<i>Entrer une lettre du nom</i>"]],
            ED_VALIDATE          => [ED_VALIDATE_REQUIRED],
            ED_SELECT_SEARCHABLE => true,
            ED_FIELD_WIDTH       => 12,
        ],
        "!cellRefParent" => [
            ED_TYPE  => ED_TYPE_HIDDEN,
            ED_VALUE => dbUtil()->result(dbUtil()->selectRow("members", "cellRef", "num=" . $_REQUEST["primary"][0]), 0)
        ],
    ];
    $this->selectSearch = ["ed_num"];
  }

  private function buildUpdate_InsertFields()
  {

    $this->fields += [
        ED_FORM_ATTR => "style=min-height:500px",
        "!isInsert"  => [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => $this->isInsert || $this->addNew],
        "firstName"  => [
            ED_LABEL       => "Prénom",
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_VALIDATE    => [ED_VALIDATE_MAX_LENGTH => 20, ED_VALIDATE_REQUIRED => true, ED_VALIDATE_INVALIDE => "Manquant"],
            ED_FIELD_WIDTH => 6,
        ],
        "name"       => [
            ED_LABEL       => "Nom",
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_VALIDATE    => [ED_VALIDATE_MAX_LENGTH => 30, ED_VALIDATE_REQUIRED => true, ED_VALIDATE_INVALIDE => "Manquant"],
            ED_FIELD_WIDTH => 6
        ],
    ];

    $this->fields += ["d0" => [
            ED_INSERT_HTML => ""
            . "<ul class='justify-content-center nav nav-tabs'>"
            . "  <li class=' nav-item'>"
            . "    <a class='nav-link active' data-toggle='tab' href='#functionTab'>Fonctions</a>"
            . "  </li>"
            . "  <li class='nav-item'>"
            . "    <a class='nav-link' data-toggle='tab' href='#adminTab'>Administratif</a>"
            . "  </li>"
            . ($this->buildCell ? ""
            . "  <li class='nav-item' id=ed_cellNav>"
            . "    <a class='nav-link' data-toggle='tab' href='#cellTab'>Cellule</a>"
            . "  </li>" : '')
            . ($this->isInsert ? ""
            . "  <li class='nav-item parent-nav' style=display:none>"
            . "    <a class='nav-link' data-toggle='tab' href='#parent1Tab'>Parent 1</a>"
            . "  </li>"
            . "  <li class='nav-item  parent-nav' style=display:none>"
            . "    <a class='nav-link' data-toggle='tab' href='#parent2Tab'>Parent 2</a>"
            . "  </li>" : '')
            . "</ul>"
            . "<div class='tab-content'>"
            . "  <div id=functionTab class='tab-pane show active'>"
    ]];
    $this->buildFunctions_TeamsFields();
    $this->fields += ["d1" => [
            ED_INSERT_HTML => ""
            . "  </div>"
            . "  <div id=adminTab class='tab-pane fade'>"
    ]];
    $this->buildAdminFields();
    if ($this->buildCell) {
      $this->fields += ["d2" => [
              ED_INSERT_HTML => ""
              . "  </div>"
              . "  <div id=cellTab class='tab-pane fade'>"
      ]];
      $this->buildCellFields();
    }
    if ($this->isInsert) {
      $this->fields += ["d3" => [
              ED_INSERT_HTML => ""
              . "  </div>"
              . "  <div id=parent1Tab class='tab-pane fade'>"
      ]];
      $this->buildParentFields(1);
      $this->fields += ["d4" => [
              ED_INSERT_HTML => ""
              . "  </div>"
              . "  <div id=parent2Tab class='tab-pane fade'>"
      ]];
      $this->buildParentFields(2);
    }
    $this->fields += ["dLast" => [
            ED_INSERT_HTML => ""
            . "  </div>"
            . "</div>"
    ]];
  }

  private function buildParentFields($num)
  {
    $this->fields += [
        "!email$num"     => [
            ED_LABEL       => "EMail",
            ED_TYPE        => "email",
            ED_FIELD_WIDTH => 8,
            ED_VALIDATE    => [ED_VALIDATE_MAX_LENGTH => 50, ED_VALIDATE_REQUIRED => $num == 1, ED_VALIDATE_INVALIDE => "Manquant ou invalide"],
        ],
        "!phone$num"     => [
            ED_LABEL       => "GSM",
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_ATTR        => "pattern='[+()\./ 0-9]{9,20}'",
            ED_FIELD_WIDTH => 4,
            ED_VALIDATE    => [ED_VALIDATE_MAX_LENGTH => 15, ED_VALIDATE_INVALIDE => "Uniquement N° tél."],
        ],
        "!firstName$num" => [
            ED_LABEL       => "Prénom",
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_VALIDATE    => [ED_VALIDATE_MAX_LENGTH => 20],
            ED_FIELD_WIDTH => 6,
        ],
        "!name$num"      => [
            ED_LABEL       => "Nom",
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_VALIDATE    => [ED_VALIDATE_MAX_LENGTH => 30],
            ED_FIELD_WIDTH => 6
        ],
    ];
  }

  private function buildAdminFields()
  {
    $affiliation = ConfigProject::get()->affiliationType;
    if ($this->isInsert || $this->addNew) {
      //remove 'A désaffilier'
      unset($affiliation[AFFILIATION_TO_REMOVE]);
    }

    $this->fields += [
        "affiliation" => [
            ED_LABEL       => "Affiliation",
            ED_TYPE        => ED_TYPE_SELECT,
            ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_INVALIDE => "Manquant"],
            ED_FIELD_WIDTH => 6,
            ED_PLACEHOLDER => '',
            ED_OPTIONS     => mdbCompos()->getOptions($affiliation, "class=pb-0")
        ],
        "sex"         => [
            ED_LABEL       => "Genre",
            ED_PLACEHOLDER => '',
            ED_TYPE        => ED_TYPE_SELECT,
            ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_INVALIDE => "Manquant"],
            ED_FIELD_WIDTH => 3,
            ED_OPTIONS     => mdbCompos()->getOptions(['M' => "Masculin", 'F' => "Féminin"]),
        ],
        "sz"          => [
            ED_LABEL       => "Taille",
            ED_PLACEHOLDER => '',
            ED_TYPE        => ED_TYPE_SELECT,
            ED_FIELD_WIDTH => 3,
            ED_OPTIONS     => mdbCompos()->getOptions([null => "(Non assigné)"] + ConfigProject::get(CONFIG_MEMBERS_SIZES)->getNameListById(), "class=pb-0")
        ],
        "email"       => [
            ED_LABEL       => "EMail",
            ED_TYPE        => "email",
            ED_FIELD_WIDTH => 8,
            ED_VALIDATE    => [ED_VALIDATE_MAX_LENGTH => 50, ED_VALIDATE_REQUIRED => true, ED_VALIDATE_INVALIDE => "Manquant ou invalide"],
        ],
        "phone"       => [
            ED_LABEL       => "GSM",
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_ATTR        => "pattern='[+()\./ 0-9]{9,20}'",
            ED_FIELD_WIDTH => 4,
            ED_VALIDATE    => [ED_VALIDATE_MAX_LENGTH => 15, ED_VALIDATE_INVALIDE => "Uniquement N° tél."],
        ],
        "remark"      => [
            ED_LABEL       => "Remarque",
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_FIELD_WIDTH => 12,
            ED_VALIDATE    => [ED_VALIDATE_MAX_LENGTH => 100],
        ],
        "birth"       => [
            ED_TYPE                 => ED_TYPE_DATETIME,
            ED_TEXT_SELECT_ON_FOCUS => true,
            ED_LABEL                => "Date Naissance",
            ED_DATETIME_PICKER      => ["format" => "L"],
            ED_FIELD_WIDTH          => 3,
            ED_VALIDATE             => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_INVALIDE => "Age compris entre 3 et 100 ans"],
        ],
        "birthLoc"    => [
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_LABEL       => "Lieu Naissance",
            ED_FIELD_WIDTH => 6,
            ED_VALIDATE    => [ED_VALIDATE_MAX_LENGTH => 50],
        ],
        "NatNum"      => [
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_LABEL       => "N° national",
            ED_FIELD_WIDTH => 3,
            ED_VALIDATE    => [ED_VALIDATE_MAX_LENGTH => 10],
        ],
    ];
  }

  private function buildFunctions_TeamsFields()
  {
    if (!$this->row["functions"]) {
      $this->buildFunction_TeamFields(null);
    }
    else {
      foreach ($this->row["functions"] as $function) {
        if ($this->row["allTeams"][$function]) {
          foreach ($this->row["allTeams"][$function] as $team) {
            $this->buildFunction_TeamFields($function, $team);
          }
        }
        else {
          $this->buildFunction_TeamFields($function);
        }
      }
    }
  }

  private function buildFunction_TeamFields($function, $team = null, $id = null)
  {
    static $i = 0;
    $i = $id ?? $i + 1;
    $this->fields += [
        "!func$i"      => [
            ED_LABEL                  => "Fonction",
            ED_TYPE                   => ED_TYPE_SELECT,
            ED_SELECT_SEARCHABLE      => true,
            ED_SELECT_VISIBLE_OPTIONS => 8,
            ED_OPTIONS                => mdbCompos()->getOptions(ConfigProject::get(CONFIG_MEMBERS_FUNCTIONS)->getNameListById(true), function ($function) {
              return "class=pb-0 data-role=" . ConfigProject::get()->getFunctionRole($function);
            }),
            ED_VALUE       => $function,
            ED_PLACEHOLDER => '',
            ED_FIELD_WIDTH => 5,
            ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true],
                    
        ],
        "!team$i"     => [
            ED_VALUE       => intdiv($team, 10),
            ED_LABEL       => "Equipe",
            ED_PLACEHOLDER => "",
            ED_TYPE        => ED_TYPE_SELECT,
            ED_OPTIONS     => mdbCompos()->getOptions(ConfigProject::get(CONFIG_MEMBERS_CATEGORIES)->getActiveListPropById("short"), "class=pb-0"),
            ED_DIV_ATTR    => !$team ? " style = display:none" : '',
            ED_FIELD_WIDTH => 3
        ],
        "!tSet$i"     => [
            ED_VALUE       => $team % 10,
            ED_LABEL       => "Rang",
            ED_TYPE        => ED_TYPE_SELECT,
            ED_OPTIONS     => $this->getTeamSetOptions($team, "class=pb-0"),
            ED_DIV_ATTR    => !$team ? " style = display:none" : '',
            ED_FIELD_WIDTH => 2,
        ],
        "!action$i"   => [
            ED_TYPE        => ED_TYPE_TEXT,
            ED_CONTENT     => "<a class = 'ml-2 " . (!$function ? "disabled " : '') . "icon-action function-add'><i class = 'fas fa-plus-circle fa-lg grey-text' title = 'Ajouter fonction' ></i></a>"
            . "<a class = 'icon-action function-remove mr-0'><i class = 'fas fa-minus-circle fa-lg grey-text' title = 'Supprimer fonction'></i></a>",
            ED_FIELD_WIDTH => 2,
        ],
    ];
    if ($this->isInsert) {
      //remove functions where role is parent
      foreach ($this->fields["!func$i"][ED_OPTIONS] as $idx => $ar) {
        if ($ar[ED_ATTR] == "data-role=" . ROLE_PARENT) {
          unset($this->fields["!func$i"][ED_OPTIONS][$idx]);
        }
      }
    }
  }

  private function getTeamSetOptions($team)
  {
    $ar = [0 => "&nbsp;
        "];
    for ($char = 'A'; $char < 'G'; $char = chr(ord($char) + 1)) {
      $ar += [ord($char) - ord('A') + 1 => $char];
    }
    return mdbCompos()->getOptions($ar);
  }

  private function buildCellFields()
  {
    $this->fields += [
        "!cellVisible" => [
            ED_TYPE  => ED_TYPE_HIDDEN,
            ED_VALUE => true,
        ],
        "!city"        => [
            ED_LABEL             => "Commune",
            ED_TYPE              => ED_TYPE_SELECT,
            ED_OPTIONS           => $this->getCityOptions(),
            ED_VALIDATE          => [ED_VALIDATE_REQUIRED],
            ED_SELECT_SEARCHABLE => true,
            ED_FIELD_WIDTH       => 12,
            ED_VALIDATE          => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_INVALIDE => "Manquant"],
        ],
        "!address"     => [
            ED_LABEL             => "Rue",
            ED_TYPE              => ED_TYPE_SELECT,
            ED_OPTIONS           => $this->getAddressOptions(),
            ED_VALIDATE          => [ED_VALIDATE_REQUIRED],
            ED_SELECT_SEARCHABLE => true,
            ED_DISABLED          => !$this->row["city"],
            ED_FIELD_WIDTH       => 8,
        ],
        "!buildingNb"  => [
            ED_LABEL         => "N°",
            ED_TYPE          => ED_TYPE_ALPHA,
            ED_VALIDATE      => [ED_VALIDATE_MAX_LENGTH => 5],
            ED_FIELD_WIDTH   => 2,
            ED_DISABLED      => !$this->row["city"],
        ],
        "!box"         => [
            ED_LABEL         => "Bte",
            ED_TYPE          => ED_TYPE_ALPHA,
            ED_VALIDATE      => [ED_VALIDATE_MAX_LENGTH => 5],
            ED_FIELD_WIDTH   => 2,
            ED_DISABLED      => !$this->row["city"],
        ],
    ];
    $this->selectSearch = ["ed_address", "ed_city"];
  }

  private function getCityOptions()
  {
    if (!($this->row["cellRef"]) || !$this->row["city"]) {
      return;
    }
    $res = dbUtil()->query("select a.ri, concat(a.zipCode, ' ', a.name, if(b.ri is null or a.name = b.name, '', concat(' (', b.name, ')'))) "
            . "from cities a left join cities b on b.ri = a.mainCity where a.ri = " . $this->row["city"] . " or a.ri = " . $this->row["mainCity"]);
    return mdbCompos()->getOptions($res, "class=pb-0");
  }

  private function getAddressOptions()
  {
    if (!($this->row["cellRef"]) || !$this->row["city"]) {
      return;
    }
    return mdbCompos()->getOptions(dbUtil()->selectRow("streets", "id, name", "id = " . $this->row["streetId"]), "class=pb-0");
  }

  static function axResetMembers()
  {
    return mdbCompos()->buildSelect([
                ED_NAME              => "!address",
                ED_LABEL             => "Rue",
                ED_TYPE              => ED_TYPE_SELECT,
                ED_VALIDATE          => [ED_VALIDATE_REQUIRED],
                ED_SELECT_SEARCHABLE => true,
    ]);
  }

  function axBuildNewFunctionRow()
  {
    $this->buildFunction_TeamFields(null, null, $_REQUEST["id"]);
    echo BuildForm::getForm($this->fields, null, ED_NO_FORM);
  }

  function axBuildSelectSearch()
  {
    $search = dbUtil()->real_escape_string($_REQUEST["search"]);
    switch ($_REQUEST["selectId"]) {
      case "ed_city":
        $res = dbUtil()->query("select a.ri, concat(a.zipCode, ' ', a.name, if(b.ri is null or a.name = b.name, '', concat(' (', b.name, ')'))) "
                . "from cities a left join cities b on b.ri = a.mainCity "
                . "where a.name like '%$search%' or a.zipcode like '%$search%' order by a.name");
        if (mysqli_num_rows($res) < 100) {
          if (mysqli_num_rows($res) == 0) {
            echo "-1:Rien trouvé";
            return;
          }
          echo mdbCompos()->buildSelect([
              ED_NAME              => "!city",
              ED_LABEL             => "Commune",
              ED_TYPE              => ED_TYPE_SELECT,
              ED_VALUE             => $this->row["city"],
              ED_OPTIONS           => mdbCompos()->getOptions($res, "class=pb-0"),
              ED_VALIDATE          => [ED_VALIDATE_REQUIRED => true],
              ED_SELECT_SEARCHABLE => true,
          ]);
        }
        else {
          echo "-1:Trop de choix";
        }
        return;

      case "ed_address":
        if (!$_REQUEST["city"]) {
          msgBox("La commune doit être renseignée", "Incomplet", MODAL_SIZE_SMALL, [MSGBOX_MODAL_ATTR => [MODAL_CENTERED => true, MODAL_NO_BACKDROP => true]]);
          return;
        }
        $attr = !($mainCity = dbUtil()->result(dbUtil()->selectRow("cities", "mainCity", "ri = " . $_REQUEST["city"]), 0)) || $mainCity == $_REQUEST["city"] ? "mainCity" : "city";
        $res = dbUtil()->selectRow("streets", "id, name", "$attr = " . $_REQUEST["city"] . " and name like '%$search%' order by name");
        if (mysqli_num_rows($res) < 100) {
          if (mysqli_num_rows($res) == 0) {
            echo "-1:Rien trouvé";
            return;
          }
          echo mdbCompos()->buildSelect([
              ED_NAME              => "!address",
              ED_LABEL             => "Rue",
              ED_TYPE              => ED_TYPE_SELECT,
              ED_OPTIONS           => mdbCompos()->getOptions($res, "class=pb-0"),
              ED_VALIDATE          => [ED_VALIDATE_REQUIRED => true],
              ED_SELECT_SEARCHABLE => true,
          ]);
        }
        else {
          echo "-1:Trop de choix";
        }
        return;

      case "ed_num":
        $membersTab = dbUtil()->getTableName("members");
        $membersSeasonTab = dbUtil()->getTableName("membersSeason");
        $res = dbUtil()->query("select num, concat(name, ' ', firstName) from $membersTab left join $membersSeasonTab on ms.ri=mb.num "
                . "where ms.season=" . ConfigFinances::get()->yearSeason . " and cellRefParent is null and concat(name, ' ', firstName) like '%$search%' "
                . "and functions like '%" . FUNCTION_PARENT . "%' and cellRef<>" . $_REQUEST["cellRefParent"]
                . " order by name, firstName", 1);
        if (mysqli_num_rows($res) < 50) {
          if (mysqli_num_rows($res) == 0) {
            echo "-1:Rien trouvé";
            return;
          }
          echo mdbCompos()->buildSelect([
              ED_NAME              => "!num",
              ED_LABEL             => "Nom",
              ED_TYPE              => ED_TYPE_SELECT,
              ED_OPTIONS           => mdbCompos()->getOptions($res, "class=pb-0"),
              ED_VALIDATE          => [ED_VALIDATE_REQUIRED => true],
              ED_SELECT_SEARCHABLE => true,
          ]);
        }
        else {
          echo "-1:Trop de choix";
        }
        return;
    }
  }

}
