<?php

function buildMain()
{
  echo ""
  . "<main id='main'>";
  new CitiesBox;
  echo ""
  . "</main>";
}

class CitiesBox
{

  public function __construct()
  {
    $table = new Cities("cities");
    echo ""
    . "<div id=citiesBox class='container mt-3'>";

    $table->settings[TABLE_ATTR_FAST_SEARCH_ENABLE] = true;
    $table->buildFullTable();

    echo ""
    . "</div>";
    utils()->insertReadyFunction("tableListener", "cities");
  }

}

class Cities extends BuildTable
{

  static function buildTableDefs()
  {
    dbUtil()->tables["cities"][DB_COLS_DEFINITION] = [
        "ri"       => [COL_TITLE => "RI", COL_PRIMARY => true, COL_TD_ACTIONS => true],
        "zipCode"  => [COL_TITLE => "Code postal"],
        "name"     => [COL_TITLE => "Nom"],
        "mainCity" => [COL_TITLE => "Principale", COL_EXT_REF => dbUtil()->getTableName("cities") . " a", COL_DB => "a.name"]
    ];
  }

  function __construct($tableId, $content = null)
  {
    $this::buildTableDefs();
    parent::__construct($tableId, $content);
  }

  protected function getDisplayValue($key, $row)
  {
    if ($key == "isMain" && $row[$key]) {
      return "X";
    }
    return parent::getDisplayValue($key, $row);
  }

  protected function getLimit()
  {
    return 200;
  }

}

function axCitiesEdit()
{
  switch (utils()->action) {
    case "update" :
    case "insert" :
      new CitiesEdit();
      return;

    case "delete":
      msgBox("<p>Confirmation suppression</p>", "Suppression", MODAL_SIZE_SMALL, [MSGBOX_BUTTON_ACTION => "Supprimer", MSGBOX_BUTTON_CLOSE => "Annuler"]);
      return;
  }
}

function axCitiesSubmit()
{
  switch (utils()->action) {
    case "update" :
      !$_REQUEST["val_isMain"] && $_REQUEST["val_isMain"] = "=null";
  }
  utils()->axRefreshElement("cities");
}
