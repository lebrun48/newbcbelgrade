<?php

class CitiesEdit
{

  function __construct()
  {
    Cities::buildTableDefs();
    $tuple = dbUtil()->getCurrentEditedRow();

    $fields = [
        "zipCode" => [ED_LABEL => "Code Postal", ED_TYPE => ED_TYPE_INTEGER, ED_VALIDATE => [ED_VALIDATE_MIN_VALUE => 1000, ED_VALIDATE_MAX_VALUE => 9999, ED_VALIDATE_REQUIRED => true]],
        "name"    => [ED_LABEL => "Commune", ED_TYPE => ED_TYPE_ALPHA, ED_VALIDATE => [ED_VALIDATE_MAX_LENGTH => 500, ED_VALIDATE_REQUIRED => true]],
        "isMain"  => [ED_LABEL => "Ville fusionée", ED_TYPE => ED_TYPE_CHECK, ED_VALUE=>1]
    ];

    msgBox(BuildForm::getForm($fields, $tuple), "Edition commune", null, [MSGBOX_BUTTON_ACTION => "Enregister", MSGBOX_BUTTON_CLOSE => "Annuler"]);
  }

}
