<?php

define('FUNCTION_PRESIDENT', 1);
define('FUNCTION_PRESIDENT_HONOR', 2);
define('FUNCTION_SECRETARY', 3);
define('FUNCTION_SECRETARY_ASSITANT', 4);
define('FUNCTION_TREASURER', 5);
define('FUNCTION_ADM', 6);
define('FUNCTION_TECH_DIR_MAXI', 7);
define('FUNCTION_TECH_DIR_MINI', 8);
define('FUNCTION_COACH', 9);
define('FUNCTION_COACH_ASSIST', 10);
define('FUNCTION_COACH_REPLACE', 11);
define('FUNCTION_PLAYER', 12);
define('FUNCTION_PLAYER_HURT', 13);
define('FUNCTION_PLAYER_SUSP', 14);
define('FUNCTION_PLAYER_MUT_OUT', 15);
define('FUNCTION_PLAYER_MUT_IN', 16);
define('FUNCTION_PLAYER_STOP', 17);
define('FUNCTION_REFEREE', 18);
define("FUNCTION_REFEREE_CLUB", 19);
define("FUNCTION_REFEREE_CASUAL", 20);
define('FUNCTION_COMITY', 21);
define('FUNCTION_COMITY_SPORT', 22);
define('FUNCTION_COMITY_TECH_ADV', 23);
define('FUNCTION_PARENT', 24);
define('FUNCTION_INTERN', 25);
define('FUNCTION_SPONSOR', 26);
define('FUNCTION_CAFET', 27);
define('FUNCTION_OTHER', 28);

class UpdateConfig
{

  public function __construct()
  {
    dbUtil()->begin_transaction();
    $this->updateMembers();
    $this->updateFinances();
    dbUtil()->commit();
  }

  function updateMembers()
  {
    //sizes
    $sizes = [["id" => 1, "name" => "6 ans"], ["id" => 2, "name" => "8 ans"], ["id" => 3, "name" => "10 ans"], ["id" => 4, "name" => "12 ans"], ["id" => 5, "name" => "XS"], ["id" => 6, "name" => "14 S"], ["id" => 7, "name" => "M"], ["id" => 8, "name" => "L"], ["id" => 9, "name" => "XL"], ["id" => 10, "name" => "XXL"], ["id" => 11, "name" => "XXXL"], ["id" => 12, "name" => "XXXXL"]];
    $this->saveConfig($sizes, CONFIG_MEMBERS_SIZES);

    //functions
    $functions = [
        ["id" => FUNCTION_PLAYER, "name" => "Joueur", "role" => ROLE_PLAYER, "system" => CONFIG_SYSTEM_NO_DELETE],
        ["id" => FUNCTION_PLAYER_HURT, "name" => "Blessé", "role" => ROLE_PLAYER],
        ["id" => FUNCTION_PLAYER_SUSP, "name" => "Suspendu", "role" => ROLE_PLAYER],
        ["id" => FUNCTION_COACH, "name" => "Entraineur", "role" => ROLE_COACH, "system" => CONFIG_SYSTEM_NO_DELETE],
        ["id" => FUNCTION_COACH_ASSIST, "name" => "Assistant", "role" => ROLE_COACH],
        ["id" => FUNCTION_COACH_REPLACE, "name" => "Remplaçant", "role" => ROLE_COACH],
        ["id" => FUNCTION_PARENT, "name" => "Parent", "role" => ROLE_PARENT, "system" => CONFIG_SYSTEM_NO_DELETE],
        ["id" => FUNCTION_PLAYER_STOP, "name" => "Arrêt", "role" => ROLE_OTHER],
        ["id" => FUNCTION_PLAYER_MUT_OUT, "name" => "Mutation sortie", "role" => ROLE_PLAYER],
        ["id" => FUNCTION_PLAYER_MUT_IN, "name" => "Mutation entrée", "role" => ROLE_PLAYER],
        ["id" => FUNCTION_REFEREE, "name" => "Arbitre", "role" => ROLE_OTHER],
        ["id" => FUNCTION_REFEREE_CLUB, "name" => "Arbitre club", "role" => ROLE_OTHER],
        ["id" => FUNCTION_REFEREE_CASUAL, "name" => "Arb. occasionnel", "role" => ROLE_OTHER],
        ["id" => FUNCTION_INTERN, "name" => "Stagiaire", "role" => ROLE_OTHER],
        ["id" => FUNCTION_COMITY, "name" => "Comité", "role" => ROLE_OTHER],
        ["id" => FUNCTION_COMITY_SPORT, "name" => "Comité sportif", "role" => ROLE_OTHER],
        ["id" => FUNCTION_PRESIDENT, "name" => "Président", "role" => ROLE_ADM, "system" => CONFIG_SYSTEM_NO_DELETE],
        ["id" => FUNCTION_PRESIDENT_HONOR, "name" => "Président d'honneur", "role" => ROLE_OTHER],
        ["id" => FUNCTION_ADM, "name" => "Administrateur", "role" => ROLE_ADM, "system" => CONFIG_SYSTEM_NO_DELETE],
        ["id" => FUNCTION_SECRETARY, "name" => "Secrétaire", "role" => ROLE_SECRETARY, "system" => CONFIG_SYSTEM_NO_DELETE],
        ["id" => FUNCTION_SECRETARY_ASSITANT, "name" => "Secrétaire adjoint", "role" => ROLE_SECRETARY],
        ["id" => FUNCTION_TREASURER, "name" => "Trésorier", "role" => ROLE_TREASURER, "system" => CONFIG_SYSTEM_NO_DELETE],
        ["id" => FUNCTION_TECH_DIR_MAXI, "name" => "Direct. tech. maxi", "role" => ROLE_OTHER],
        ["id" => FUNCTION_TECH_DIR_MINI, "name" => "Direct. tech. mini", "role" => ROLE_OTHER],
        ["id" => FUNCTION_COMITY_TECH_ADV, "name" => "Conseiller tech.", "role" => ROLE_OTHER],
        ["id" => FUNCTION_SPONSOR, "name" => "Sponsor", "role" => ROLE_OTHER],
        ["id" => FUNCTION_CAFET, "name" => "Cafétéria", "role" => ROLE_OTHER],
        ["id" => FUNCTION_OTHER, "name" => "Autre", "role" => ROLE_OTHER, "system" => CONFIG_SYSTEM_NO_DELETE],
    ];
    usort($functions, function ($a, $b) {
      return $a["id"] - $b["id"];
    });
    $this->saveConfig($functions, CONFIG_MEMBERS_FUNCTIONS);

    //categories
    $allCategories = dbUtil()->fetch_all(dbUtil()->selectRow("category", "id, short, name, if(disabled=1,null,1) as active, ageMin, ageMax, code"), MYSQLI_ASSOC);
    $this->saveConfig($allCategories, CONFIG_MEMBERS_CATEGORIES);
  }

  function updateFinances()
  {
    //config
    dbUtil()->seeRequest = 1;
    ConfigFinances::get();

    $categories = array(
        1   => array(0 => array("Entraineurs", 103, 2)),
        2   => array(0 => array("Cotisations", 100, 100)),
        3   => array(0 => array("Location Salles", 2, 2)),
        4   => array(0  => array("Manifestations", 103, 0),
            13 => array("Stage rentrée", 103, 2),
            14 => array("Stage Xmas", 103, 2),
            18 => array("Soirée Xmas", 103, 0),
            29 => array("Stage Carnaval", 103, 2),
            1  => array("Stage Pâques", 103, 2),
            27 => array("Repas photo club", 103, 0),
            6  => array("St Nicolas", 103, 0),
            23 => array("Raclette", 103, 0),
            28 => array("Repas boulettes", 103, 0),
            10 => array("Gaufres", 103, 0),
            5  => array("Lasagnes", 103, 0),
            15 => array("Marche ADEPS", 103, 0),
            26 => array("Playoffs", 103, 0),
            8  => array("Tournois", 103, 0),
            19 => array("Coupe prov.", 103, 0),
            20 => array("BAB/BIP/BEN", 103, 0),
            21 => array("Spectacle", 103, 0, 1150),
            22 => array("Fête basket", 103, 0),
            9  => array("Tombola", 103, 0),
            17 => array("3 contre 3", 103, 0),
            25 => array("All Star Games", 103, 0),
            11 => array("Vins Alsavins", 103, 0),
            12 => array("Boutique", 103, 0),
            16 => array("Autre", 103, 0),
            3  => array("Gibier", 103, 0),
            2  => array("BBQ", 103, 0),
            24 => array("JRJ", 103, 0),
            4  => array("Choucroute", 103, 0),
            7  => array("VTT", 103, 0)),
        5   => array(0 => array("Buvette", 103, 0),
            1 => array("Recettes", 103, 103),
            2 => array("Marchandises", 0, 0),
            3 => array("Déf. bénévoles", 2, 2),
            4 => array("Gérant", -1, -1),
            5 => array("Autre", 103, 0),
            6 => array("Part séniors", 103, 0)),
        6   => array(0 => array("Arbitres", 2, 2)),
        7   => array(0 => array("Matériel-Equipement", 0, 2, -1),
            1 => array("Réserve remplacement", -1, -1)),
        8   => array(0  => array("Adminsitratif", 2, 2, -2),
            1  => array("Secrétariat", 103, 2),
            2  => array("Comptabilité/trésorerie", 103, 2),
            3  => array("Poste", 2, 2),
            4  => array("Dir. Technique", 103, 2),
            6  => array("Portail et site Internet", 2, 2),
            7  => array("Matériel-Entretien", 2, 2),
            8  => array("Banque", 103, 2),
            9  => array("Assurances", 2, 2),
            10 => array("Autre", 103, 3),
            11 => array("Pertes/profits", 103, 3)),
        9   => array(0 => array("Sponsors", 103, 2, -151),
            1 => array("Rétrocession", -1, -1),
            2 => array("Panneaux", 103, 2)),
        10  => array(0 => array("AWBB", 102, 2),
            1 => array("Transferts", 103, 2),
            2 => array("Subsides", 102, 102),
            3 => array("Amendes", 2, 2),
            4 => array("Frais", 2, 2),
            5 => array("Licence col.", 2, 2),
            6 => array("Compensations", 103, 2)),
        11  => array(0 => array("Virements internes", -1, -1),
            1 => array("Attente", -1, -1),
            2 => array("Compte", -1, -1),
            3 => array("Bons Mestdagh", -1, -1)),
        12  => array(0 => array("Subsides", 102, 102),
            1 => array("Ville Namur", 102, 102, 100),
            2 => array("Ville Namur: jeunes", 102, 102),
            3 => array("Région", 102, 102)),
        13  => array(0  => array("Séniors", -1, -1, -100),
            10 => array("Défraiements coach", 103, 2),
            11 => array("Défraiements assistant", 103, 2),
            1  => array("Défraiements joueurs", 103, 2),
            12 => array("Arbitres", 103, 2),
            13 => array("Salles", 103, 2),
            16 => array("AWBB", 103, 2),
            3  => array("Festivités", 103, 0),
            4  => array("Stage rentrée", 103, 2),
            5  => array("Play-Off", 103, 0),
            6  => array("Déplacement car", 2, 2),
            7  => array("Kiné", 2, 2),
            9  => array("Entr. Phy.", 2, 2),
            8  => array("Restauration", 2, 2),
            2  => array("Entrées", 103, 103),
            14 => array("Buvette", 103, 103),
            15 => array("Subsides", 103, 103)),
        14  => array(0 => array("Dettes", -1, -1),
            3 => array("Caution", 602, 602)),
        20  => array(0 => array("Réserve", -1, -1),
            1 => array("Equipement", -1, -1)),
        15  => array(0 => array("Cadeaux St Nicolas", 103, 2)),
        17  => array(0 => array("Déf. Bénévoles", 103, 2)),
        16  => array(0 => array("Report", -1, -1)),
        19  => array(0 => array("TVA", -1, -1),
            1 => array("Acompte", -1, -1)),
        127 => array(0 => array("Clôture compta", -1, -1)),
    );
//define('CATEGORY_TRANSACTION', 11 * 256);
//define('CATEGORY_LASTCLOSURE', 127 * 256);
//define('CATEGORY_MAIN_TRANSACTION', 11);
//define('CATEGORY_MAIN_TVA', 19);
//define('CATEGORY_MAIN_DEBTS', 14);

    $id = 10;
    $index=0;
    foreach ($categories as $idxMain => $main) {
      foreach ($main as $idxSub => $sub) {
        $idxSub == 0 && $parent = $index;
        $cat = $idxMain * 256 + $idxSub;
        $row = ["id" => $id, "active" => 1, "name" => $sub[0], "parent" => $idxSub == 0 ? -1 : $parent];
        ($v = $sub[1]) && $v != -1 && $row["resultPos"] = $v;
        ($v = $sub[2]) && $v != -1 && $row["resultNeg"] = $v;
        (
                $idxMain == 11 && $idxSub == 0 && ($row["id"] = CATEGORY_MAIN_TRANSACTION) ||
                $idxMain == 14 && $idxSub == 0 && ($row["id"] = CATEGORY_MAIN_DEBTS) 
                )&& ($row["system"] = CONFIG_SYSTEM_NO_DELETE) && $id--;
        (
                $idxMain == 16 && $idxSub == 0 && ($row["id"] = CATEGORY_MAIN_REPORT) ||
                $idxMain == 127 && $idxSub == 0 && ($row["id"] = CATEGORY_LASTCLOSURE) ||
                $idxMain == 19 && ($row["id"] = CATEGORY_MAIN_TVA + $idxSub)
                ) && ($row["system"] = CONFIG_SYSTEM_HIDE) && $id--;
        dbUtil()->updateRow("biller", "category=" . $row["id"], "category=$cat");
        dbUtil()->updateRow("budget", "category=" . $row["id"], "category=$cat");
        dbUtil()->updateRow("billerClassify", "category=" . $row["id"], "category=$cat");
        $nCategories[] = $row;
        $id++;
        $index++;
      }
    }
    $this->saveConfig($nCategories, CONFIG_FINANCES_CATEGORIES);
  }

  function saveConfig($val, $key)
  {
    dbUtil()->query("insert into config set val='" . dbUtil()->getDbCnx()->real_escape_string(json_encode($val)) . "', ri=$key on duplicate key update val=values(val)");
    dbUtil()->deleteRow("config_orders", "keyIdx=$key");
    foreach ($val as $order => $val) {
      dbUtil()->insertRow("config_orders", ["keyIdx" => $key, "id" => $val["id"], "orderId" => $order]);
    }
  }

}
