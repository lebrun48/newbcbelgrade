<?php

class UpdateMembers
{

  function __construct()
  {
    dbUtil()->seeRequest = 1;

    dbUtil()->query("delete from members where type=0");
    dbUtil()->query("ALTER TABLE `members` CHANGE `phP` `phone` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL");
    dbUtil()->query("ALTER TABLE `members` CHANGE `emailP` `email` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL");
    dbUtil()->query("ALTER TABLE `members` CHANGE `birth` `birth` VARCHAR(10) NULL DEFAULT NULL");
    dbUtil()->query("update members set birth=null where birth='0000-00-00'");
    dbUtil()->query("ALTER TABLE `members` CHANGE `birth` `birth` DATE NULL DEFAULT NULL");
    dbUtil()->query("ALTER TABLE `members` ADD `cellRef` MEDIUMINT NULL AFTER `num`");
    dbUtil()->query("ALTER TABLE `members` ADD `cellRefParent` MEDIUMINT NULL AFTER `cellRef`");
    dbUtil()->query("ALTER TABLE `members` ADD `affiliation` TINYINT NOT NULL DEFAULT '0' AFTER `cellRefParent`");
    dbUtil()->query("ALTER TABLE `members` CHANGE `sex` `sex` CHAR(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL");
    dbUtil()->query("ALTER TABLE `members` CHANGE `num` `num` INT(3) NOT NULL AUTO_INCREMENT");
    dbUtil()->query("ALTER TABLE `members` CHANGE `visibility` `publicAttr` VARCHAR(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL");
    dbUtil()->query("update members set affiliation = 2 where extern is not null");
    dbUtil()->query("update members set affiliation = 1 where extern is null");
    dbUtil()->query("ALTER TABLE `members` ADD `roles` varchar(200) DEFAULT NULL after `dateIn`");
    dbUtil()->query("ALTER TABLE `members` ADD `allowTrace` tinyint(4) DEFAULT NULL after `dateIn`");
    dbUtil()->query("ALTER TABLE `members` ADD `state` tinyint(4) NOT NULL DEFAULT '0' after `dateIn`");
    dbUtil()->query("ALTER TABLE `members` ADD `token` char(64) DEFAULT NULL after `dateIn`");
    dbUtil()->query("ALTER TABLE `members` ADD `lastLogin` datetime DEFAULT NULL after `dateIn`");
    dbUtil()->query("ALTER TABLE `members` ADD `updpwd` bigint(20) DEFAULT NULL after `dateIn`");
    dbUtil()->query("ALTER TABLE `members` ADD `nUpd` tinyint(4) DEFAULT NULL after `dateIn`");
    dbUtil()->query("ALTER TABLE `members` ADD `pwd` char(255) DEFAULT NULL after `dateIn`");
    dbUtil()->query("ALTER TABLE `members` ADD `demoUser` smallint(6) NOT NULL DEFAULT '0' after `dateIn`");

    dbUtil()->query("truncate table cells");
    dbUtil()->query("update members set cellRef=null");

    $res = dbUtil()->selectRow("members", "num, address, cp, town", "address is not null");
    while ($tup = dbUtil()->fetch_assoc($res)) {
      debugLog($tup, "orig");
      debugLog(bin2hex($tmp = $tup["address"]));
      $tmp = str_replace('/', " / ", $tmp);
      $tmp = str_replace('n°', " ", $tmp);
      $tmp = str_replace('-', " ", $tmp);
      $tmp = mb_strtolower(str_replace('.', " ", $tmp));
      $word = strtok($tmp, "\n\r ,");
      do {
        debugLog($word);
        if (!$prev && is_numeric($word[0])) {
          $streetNb = $word;
          $prev = "nb";
          continue;
        }
        if ($word == '/') {
          $prev = "box";
          continue;
        }
        if (substr($word, 0, 3) == "bte" && is_numeric($word[3])) {
          $box = substr($word, 3);
          unset($prev);
          continue;
        }
        $word == "boite" && $word = '/';
        $word == "boîte" && $word = '/';
        $word == "bte" && $word = '/';
        if ($word == '/' && $prev = "box") {
          continue;
        }
        if (strlen($word) == 1 && $word != 'r' && $prev == "nb") {
          $prev = null;
          $streetNb .= $word;
          continue;
        }
        if ($prev == "box") {
          $prev = null;
          $box = $word;
          continue;
        }
        $word == "st" && ($word = "saint");
        $word == "av" && ($word = "avenue");
        $word == "ch" && ($word = "chaussée");
        $word == "chée" && ($word = "chaussée");
        $word == "bd" && ($word = "boulevard");
        $word == "pl" && ($word = "place");
        $word == "rue" && ($word = "rue");
        $word == "r" && ($word = "rue");
        $word == "chin" && ($word = "chemin");
        ($word == "ier" || $word == "1er") && ($word = "%");
        $street .= "%$word";
        debugLog($street);
      } while ($word = strtok("\n\r ,"));
      unset($prev);
      $street = substr($street, 1);
      $city = str_replace(' ', '%', $tup["town"]);
      $city = str_replace('-', '%', $city);
      $city = str_replace('S/S', 'sur%sambre', $city);
      $city = dbUtil()->fetch_all(dbUtil()->selectRow("cities", "ri, mainCity", "name like '" . dbUtil()->real_escape_string($city) . "'"));
      $mainCity = implode(array_column($city, 1), ",");
      $city = implode(array_column($city, 0), ",");
      $streetRI = dbUtil()->fetch_all(dbUtil()->selectRow("streets", "id", "name like '" . dbUtil()->real_escape_string($street) . "' and (city in ($city) or mainCity in ($city)" . ($mainCity ? " or mainCity in ($mainCity))" : ')')));
      if (sizeof($streetRI) > 1) {
        $streetRI = dbUtil()->fetch_all(dbUtil()->selectRow("streets", "id", "name like '" . dbUtil()->real_escape_string(str_replace('%', ' ', $street)) . "' and (city in ($city) or mainCity in ($city)" . ($mainCity ? " or mainCity in ($mainCity))" : ')')));
      }
      !isset($streetNb) && $streetNb = '0';
      if (isset($streetNb) && $streetRI && sizeof($streetRI) == 1) {
        try {
          dbUtil()->insertRow("cells", [
              "streetID"   => $streetRI[0][0],
              "buildingNb" => "='" . strtoupper($streetNb) . "'",
              "box"        => "='" . strtoupper($box) . "'"]);
        } catch (Exception $exc) {
          echo "duplicate<br>";
          if ($exc->getCode() == E_DB_DUPLICATE) {
            $cell = dbUtil()->result(dbUtil()->query("select ri from cells where streetID=" . $streetRI[0][0] . " and buildingNb='" . strtoupper($streetNb) . "' and box='" . strtoupper($box) . "'"), 0);
            //dbUtil()->insertRow("cells", ["cell" => $cell, "street_name" => $tup["address"]]);
          }
          else
            throw new Exception($exc);
        }
        dbUtil()->query("update members set cellRef=" . ($cell ?? dbUtil()->getDbCnx()->insert_id) . " where num=" . $tup["num"]);
        unset($cell);
      }
      else {
        debugLog("ERROR: $street, $streetNb");
      }
      unset($box);
      unset($street);
      unset($streetNb);
      unset($city);
    }
    dbUtil()->query("insert into members set sex=null, sz=null, email=null, phone=null, remark=null, birth=null, birthLoc=null, NatNum=null, firstName='Jacques', name='Root', affiliation='0', num=-1");
    dbUtil()->query("insert into members_season set team=null, allTeams=null, function='17', functions='-17-', ri='-1', season=" . ConfigFinances::get()->yearSeason);

    //dbUtil()->query("update members join cells on cells.street_name=members.address set cellRef=ri where cells.streetID is not null");
    //dbUtil()->query("update members join cells on cells.street_name=members.address set cellRef=cell where cells.streetID is null");
    //dbUtil()->query("ALTER TABLE `bcbelgrade`.`cells` ADD UNIQUE `CELL` (`streetID`, `buildingNb`, `box`)");
  }

}
