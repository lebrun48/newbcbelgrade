<?php

class UpdateFinances
{

  public function __construct()
  {
    dbUtil()->seeRequest = 1;

    dbUtil()->query("delete from billerNbcb where ri =23866");
    dbUtil()->query("delete FROM `billerNBCB` where ri=19226");
    dbUtil()->query("ALTER TABLE `billerNBCB` CHANGE `account` `account` SMALLINT(4) NOT NULL");
    dbUtil()->query("ALTER TABLE `billerNBCB` ADD `rowDivision` INT NULL AFTER `subpart`");
    dbUtil()->query("ALTER TABLE `billerNBCB` ADD `subpartNb` TINYINT NULL AFTER `subpart`");
    dbUtil()->query("ALTER TABLE `billerNBCB` ADD `crc32` VARCHAR(8) NULL AFTER `accountNb`");
    dbUtil()->query("ALTER TABLE `billerNBCB` CHANGE `category` `category` SMALLINT(6) NULL");
    dbUtil()->query("ALTER TABLE `billerNBCB` CHANGE `season` `season` SMALLINT(4) NOT NULL");
    dbUtil()->query("update `billerNBCB`set account=account+250 where account>=50");
    dbUtil()->query("update `billerNBCB`set account=200 where account=20");
    dbUtil()->query("update `billerNBCB`set account=201 where account=300 and hidden=1");
    dbUtil()->query("update `billerNBCB`set account=202 where account=21");
    dbUtil()->query("update `billerNBCB`set account=account+90 where account>=10 and account<100");
    dbUtil()->query("update billerNBCB set season=season+2000");
    dbUtil()->query("INSERT INTO `billerNBCB` (`season`, `account`, `date`, `num`, `orig`, `subpart`, `detail`, `amount`, `TVA`, `TVAType`, `TVA_status`, `status`, `ri`, `category`, `hidden`, `accountNb`) VALUES ('2021', '200', '2021-12-31', NULL, 'Clôture comptable année 2020', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, '32512', '0', NULL)");
    dbUtil()->query("ALTER TABLE `billerNBCB` ADD INDEX (`rowDivision`)");
    dbUtil()->query("ALTER TABLE `billerNBCB` ADD INDEX (`date`)");
    dbUtil()->query("ALTER TABLE `billerNBCB` ADD INDEX (`crc32`)");
    dbUtil()->query("ALTER TABLE `billernbcb` ADD INDEX (`status`)");
    dbUtil()->query("ALTER TABLE `billernbcb` ADD INDEX (`category`)");
    dbUtil()->query("update billerNBCB set crc32=hex(crc32(concat(amount,date,ifnull(detail, ''),ifnull(orig,'')))) where account=0 order by date desc, ri desc limit 1");
    dbUtil()->query("update billerNBCB set crc32=hex(crc32(concat(amount,date,ifnull(detail, ''),ifnull(orig,'')))) where account=1 order by date desc, ri desc limit 1");
    dbUtil()->query("update billerNBCB set crc32=hex(crc32(concat(amount,date,ifnull(detail, ''),ifnull(orig,'')))) where account=5 order by date desc, ri desc limit 1");
    dbUtil()->query("update billerNBCB set crc32=hex(crc32(concat(amount,date,ifnull(detail, ''),ifnull(orig,'')))) where account=6 order by date desc, ri desc limit 1");
    dbUtil()->query("update `billerNBCB` set hidden=0 where hidden=1 and floor(account/100)!=1");
    dbUtil()->query("update billernbcb set TVAType=null where tva is null and tvatype is not null");
    dbUtil()->query("ALTER TABLE `billerNBCB_classify` CHANGE `content` `orig` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL");
    dbUtil()->query("ALTER TABLE `billerNBCB_classify` ADD `ri` SMALLINT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`ri`)");
    dbUtil()->query("ALTER TABLE `billerNBCB_classify` ADD `detail` VARCHAR(100) NULL AFTER `orig`");
    dbUtil()->query("ALTER TABLE `billerNBCB_classify` ADD `amount` VARCHAR(100) NULL AFTER `detail`");
    dbUtil()->query("delete FROM `billerNBCB_budget` where subBudget is not null");
    dbUtil()->query("delete from `billernbcb_budget` where category=1036 and season=15");
    dbUtil()->query("ALTER TABLE `billerNBCB_budget` CHANGE `season` `season` SMALLINT(4) NOT NULL");
    dbUtil()->query("ALTER TABLE `billerNBCB_budget` DROP `updAmount`");
    dbUtil()->query("ALTER TABLE `billerNBCB_budget` DROP INDEX `unicity`");
    dbUtil()->query("ALTER TABLE `billerNBCB_budget` DROP `subBudget`");
    dbUtil()->query("ALTER TABLE `billerNBCB_budget` CHANGE `amount` `amount` INT NOT NULL");
    dbUtil()->query("ALTER TABLE `billerNBCB_budget` CHANGE `official` `official` INT NULL DEFAULT NULL");
    dbUtil()->query("ALTER TABLE `billerNBCB_budget` ADD UNIQUE (`category`, `season`)");
    dbUtil()->query("ALTER TABLE `billerNBCB_budget` ADD `ri` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`ri`)");
    dbUtil()->query("update billerNBCB_budget set season=season+2000");
    
  }

}
