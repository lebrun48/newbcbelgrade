<?php

class BuildCities
{

  function __construct()
  {
    $handle = fopen("code-postaux-belge.csv", "r");
    $line = fgets($handle);
    $i = 1;
    while ($ar = fgetcsv($handle, 0, ";")) {
      dbutil()->insertRow("cities", "name='" . dbUtil()->real_escape_string($ar[1]) . "', zipCode=$ar[0]");
    }
    return;
  }

  function setCityInStreets()
  {
    echo "setCityInStreets<br>";
    $handle = fopen("icar_adpt.csv", "r");
    $i = 1;
    if ($offset = $_REQUEST["offset"]) {
      fseek($handle, $offset);
      $i = $_REQUEST["i"];
    }
    else {
      $line = fgets($handle);
    }
    $first = $i;
    $now = time();
    while ($line = fgets($handle)) {
      $line = mb_convert_encoding($line, "UTF-8", "Windows-1252");
      $ar = str_getcsv($line, ";");
      $streetId = $ar[9];
      $i++;
      if ($previousId == $streetId) {
        continue;
      }
      $previousId = $streetId;
      $city = dbUtil()->fetch_row(dbutil()->query("select ri,mainCity from cities where name='" . dbUtil()->real_escape_string($ar[14]) . "' order by zipCode asc limit 1"));
      if ($city) {
        if (!$city[1]) {
          dbUtil()->query("update cities set mainCity=(select mainCity from streets where id=$streetId) where ri=$city[0]");
        }
        dbUtil()->query("update streets set city=$city[0] where id=$streetId and city is null");
      }

      if (time() - $now >= 100) {
        echo "ligne $first à $i <br>";
        utils()->insertReadyFunction("continueLoad", ftell($handle), $i, utils()->action);
        return;
      }
    }
    echo "ligne $first à $i <br>";
    dbUtil()->query("update streets set city=mainCity where city is null");
  }

}
