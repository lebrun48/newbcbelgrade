<?php

class UpdateMembers_F_And_M_Fileds
{

  function __construct()
  {
    dbUtil()->seeRequest = 1;
    dbUtil()->query("update `members` left join members_season on ri=num set email=ifnull(emailF,emailM) where email is null"
            . " and (function<>" . FUNCTION_PLAYER . " and function<>" . FUNCTION_PLAYER_SUSP . " and function<>" . FUNCTION_PLAYER_HURT . " and function<>" . FUNCTION_INTERN . ')');
    dbUtil()->query("update `members` left join members_season on ri=num set phone=ifnull(phF,phM) where phone is null"
            . " and (function<>" . FUNCTION_PLAYER . " and function<>" . FUNCTION_PLAYER_SUSP . " and function<>" . FUNCTION_PLAYER_HURT . " and function<>" . FUNCTION_INTERN . ')');

    //suppress duplicate email and phones
    $res = dbUtil()->result(dbUtil()->query("select group_concat(a.num) from members a left join members b on b.email=a.emailf where a.emailf is not null and b.email is not null"), 0);
    dbUtil()->updateRow("members", "emailF=null", "num in ($res)");
    $res = dbUtil()->result(dbUtil()->query("select group_concat(a.num) from members a left join members b on b.email=a.emailM where a.emailM is not null and b.email is not null"), 0);
    dbUtil()->updateRow("members", "emailM=null", "num in ($res)");
    $res = dbUtil()->result(dbUtil()->query("select group_concat(a.num) from members a left join members b on b.phone=a.phF where a.phF is not null and b.phone is not null"), 0);
    dbUtil()->updateRow("members", "phF=null", "num in ($res)");
    $res = dbUtil()->result(dbUtil()->query("select group_concat(a.num) from members a left join members b on b.phone=a.phM where a.phM is not null and b.phone is not null"), 0);
    dbUtil()->updateRow("members", "phM=null", "num in ($res)");

    $res = dbUtil()->query("select num,cellRef,emailF, emailM, phF, phM from members left join members_season on ri=num where (function="
            . FUNCTION_PLAYER . " or function=" . FUNCTION_PLAYER_SUSP . " or function=" . FUNCTION_PLAYER_HURT . " or function=" . FUNCTION_INTERN . ") order by cellRef");
    $emailCell = [];
    $phoneCell = [];
    while ($tup = dbUtil()->fetch_assoc($res)) {
      debugLog($tup);
      $upd = '';
      if (!($cell = $tup["cellRef"])) {
        dbUtil()->insertRow("cells", ["buildingNb" => $tup["num"], "streetId" => -2]);
        $cell = dbUtil()->getDbCnx()->insert_id;
        $upd = ",cellRef=$cell";
      }
      elseif ($prevCell != $cell) {
        $prevCell = $cell;
        $emailCell = [];
        $phoneCell = [];
      }
      ($email = $tup["emailF"]) && (array_search($email, $emailCell) === false && ($emailCell[] = $email) || $email = null);
      ($phone = $tup["phF"]) && (array_search($phone, $phoneCell) === false && ($phoneCell[] = $phone) || $phone = null);
      if ($email || $phone) {
        dbUtil()->insertRow("members", ["cellRef" => $cell, "email" => $email, "phone" => $phone]);
        dbUtil()->insertRow("membersSeason", ["ri" => dbUtil()->getDbCnx()->insert_id, "function" => FUNCTION_PARENT, "functions" => '-' . FUNCTION_PARENT . '-']);
      }
      ($email = $tup["emailM"]) && (array_search($email, $emailCell) === false && ($emailCell[] = $email) || $email = null);
      ($phone = $tup["phM"]) && (array_search($phone, $phoneCell) === false && ($phoneCell[] = $phone) || $phone = null);
      if ($email || $phone) {
        dbUtil()->insertRow("members", ["cellRef" => $cell, "email" => $email, "phone" => $phone]);
        dbUtil()->insertRow("membersSeason", ["ri" => dbUtil()->getDbCnx()->insert_id, "function" => FUNCTION_PARENT, "functions" => '-' . FUNCTION_PARENT . '-']);
      }
      dbUtil()->updateRow("members", "emailF=null,emailM=null,phF=null,phM=null$upd", "num=" . $tup["num"]);
    }

    dbUtil()->query("ALTER TABLE `members` DROP INDEX `id`");
    dbUtil()->query("ALTER TABLE `members` DROP INDEX `name`");
//    dbUtil()->query("ALTER TABLE `members`"
//            . "  DROP `id`,"
//            . "  DROP `category`,"
//            . "  DROP `category2`,"
//            . "  DROP `category3`,"
//            . "  DROP `category4`,"
//            . "  DROP `category5`,"
//            . "  DROP `category6`,"
//            . "  DROP `ncategory`,"
//            . "  DROP `ncategory2`,"
//            . "  DROP `ncategory3`,"
//            . "  DROP `nCategory4`,"
//            . "  DROP `nCategory5`,"
//            . "  DROP `nCategory6`,"
//            . "  DROP `status`,"
//            . "  DROP `caution`,"
//            . "  DROP `licence`,"
//            . "  DROP `numEquip`,"
//            . "  DROP `numEquip2`,"
//            . "  DROP `numEquip3`,"
//            . "  DROP `equipSt`,"
//            . "  DROP `equipSt2`,"
//            . "  DROP `equipSt3`,"
//            . "  DROP `equipement`,"
//            . "  DROP `emailF`,"
//            . "  DROP `emailM`,"
//            . "  DROP `address`,"
//            . "  DROP `cp`,"
//            . "  DROP `town`,"
//            . "  DROP `phF`,"
//            . "  DROP `phM`,"
//            . "  DROP `phFix`,"
//            . "  DROP `type`,"
//            . "  DROP `toSelect`,"
//            . "  DROP `extern`");
    dbUtil()->query("ALTER TABLE `members` ADD FOREIGN KEY (`cellRef`) REFERENCES `cells`(`ri`) ON DELETE RESTRICT ON UPDATE NO ACTION");
    dbUtil()->query("ALTER TABLE `members` ADD FOREIGN KEY (`cellRefParent`) REFERENCES `cells`(`ri`) ON DELETE RESTRICT ON UPDATE NO ACTION");
    dbUtil()->query("ALTER TABLE `members_season` ADD FOREIGN KEY (`ri`) REFERENCES `members`(`num`) ON DELETE CASCADE ON UPDATE NO ACTION");
    dbUtil()->query("ALTER TABLE `users_bcb` ADD FOREIGN KEY (`ri`) REFERENCES `members`(`num`) ON DELETE CASCADE ON UPDATE NO ACTION");
  }
}
