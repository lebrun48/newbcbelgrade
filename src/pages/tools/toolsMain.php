<?php

function buildMain()
{
  $action = utils()->action;
  ?>
  <div id=main>
    <ul class="nav md-pills pills-unique">
      <li class="nav-item">
        <a class="nav-link<?php echo $action == "buildCities" ? " active" : '' ?>" href="?action=buildCities">buildCities</a>
      </li>
      <li class="nav-item">
        <a class="nav-link<?php echo $action == "buildStreets" ? " active" : '' ?>" href="?action=buildStreets">buildStreets</a>
      </li>
      <li class="nav-item">
        <a class="nav-link<?php echo $action == "updateFinances" ? " active" : '' ?>" href="?action=updateFinances">updateFinances</a>
      </li>
      <li class="nav-item">
        <a class="nav-link<?php echo $action == "updateMembers" ? " active" : '' ?>" href="?action=updateMembers">updateMembers</a>
      </li>
      <li class="nav-item">
        <a class="nav-link<?php echo $action == "updateMembersSeason" ? " active" : '' ?>" href="?action=updateMembersSeason">updateMembersSeason</a>
      </li>
      <li class="nav-item">
        <a class="nav-link<?php echo $action == "updateMembers_F_And_M_Fileds" ? " active" : '' ?>" href="?action=updateMembers_F_And_M_Fileds">updateMembers_F_And_M_Fileds</a>
      </li>
      <li class="nav-item">
        <a class="nav-link<?php echo $action == "updateConfig" ? " active" : '' ?>" href="?action=updateConfig">updateConfig</a>
      </li>
    </ul>
    <?php
    switch (utils()->action) {
      case "buildCities":
        new BuildCities();
        break;

      case "buildStreets":
        new BuildStreets();
        break;
      
      case "updateFinances":
        new UpdateFinances();
        break;

      case "updateMembers":
        new UpdateMembers();
        break;

      case "updateMembersSeason":
        new UpdateMembersSeason();
        break;

      case "updateMembers_F_And_M_Fileds":
        new UpdateMembers_F_And_M_Fileds();
        break;

      case "updateConfig":
        new UpdateConfig();
        break;
    }
    ?>
  </div>
  <?php
}
