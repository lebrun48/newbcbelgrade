<?php

define("CAT_FIRST", "0xFF0000=0");
define("CAT_COACH", 0x10000);
define("CAT_ASSIST", 0x20000);
define("CAT_PLAYER", 0x40000);
define("CAT_OTHERS", 0x80000);

define("CAT_REMP_COACH", (CAT_OTHERS + 10));
define("CAT_BLESSE", (CAT_OTHERS + 11));
define("CAT_REF", (CAT_OTHERS + 20));
define("CAT_REF_CLUB", (CAT_OTHERS + 30));
define("CAT_REF_CASUAL", (CAT_OTHERS + 40));
define("CAT_PARENT", (CAT_OTHERS + 50));
define("CAT_SPONSOR", (CAT_OTHERS + 60));
define("CAT_EXTERN", (CAT_OTHERS + 70));
define("CAT_CAFET", (CAT_OTHERS + 80));
define("CAT_SUSP", (CAT_OTHERS + 85));
define("CAT_MUT_OUT", (CAT_OTHERS + 90));
define("CAT_MUT_IN", (CAT_OTHERS + 91));
define("CAT_STOP", (CAT_OTHERS + 100));
define("CAT_STAGE", (CAT_OTHERS + 110));

define("CAT_ADM", 100);
define("CAT_COMITY", 200);
define("CAT_COMITY_SPORT", 300);
define("CAT_DIR_TECH_G", 400);
define("CAT_DIR_TECH_P", 500);
define("CAT_TECH_ADV", 600);
define("CAT_TRAINER", 65536);
define("CAT_TRAINER_ASSIST", 131072);
define("CAT_SENIORS", 1300);

define('FUNCTION_PRESIDENT', 1);
define('FUNCTION_PRESIDENT_HONOR', 2);
define('FUNCTION_SECRETARY', 3);
define('FUNCTION_SECRETARY_ASSITANT', 4);
define('FUNCTION_TREASURER', 5);
define('FUNCTION_ADM', 6);
define('FUNCTION_TECH_DIR_MAXI', 7);
define('FUNCTION_TECH_DIR_MINI', 8);
define('FUNCTION_COACH', 9);
define('FUNCTION_COACH_ASSIST', 10);
define('FUNCTION_COACH_REPLACE', 11);
define('FUNCTION_PLAYER', 12);
define('FUNCTION_PLAYER_HURT', 13);
define('FUNCTION_PLAYER_SUSP', 14);
define('FUNCTION_PLAYER_MUT_OUT', 15);
define('FUNCTION_PLAYER_MUT_IN', 16);
define('FUNCTION_PLAYER_STOP', 17);
define('FUNCTION_REFEREE', 18);
define("FUNCTION_REFEREE_CLUB", 19);
define("FUNCTION_REFEREE_CASUAL", 20);
define('FUNCTION_COMITY', 21);
define('FUNCTION_COMITY_SPORT', 22);
define('FUNCTION_COMITY_TECH_ADV', 23);
define('FUNCTION_PARENT', 24);
define('FUNCTION_INTERN', 25);
define('FUNCTION_SPONSOR', 26);
define('FUNCTION_CAFET', 27);
define('FUNCTION_OTHER', 28);

class UpdateMembersSeason
{

  function __construct()
  {
    dbUtil()->seeRequest = 1;
    dbUtil()->query("truncate table members_season");
    $translate = [
        CAT_REMP_COACH   => FUNCTION_COACH_REPLACE,
        CAT_BLESSE       => FUNCTION_PLAYER_HURT,
        CAT_REF          => FUNCTION_REFEREE,
        CAT_REF_CLUB     => FUNCTION_REFEREE_CLUB,
        CAT_REF_CASUAL   => FUNCTION_REFEREE_CASUAL,
        CAT_PARENT       => FUNCTION_PARENT,
        CAT_SPONSOR      => FUNCTION_SPONSOR,
        CAT_EXTERN       => FUNCTION_OTHER,
        CAT_CAFET        => FUNCTION_CAFET,
        CAT_SUSP         => FUNCTION_PLAYER_SUSP,
        CAT_MUT_OUT      => FUNCTION_PLAYER_MUT_OUT,
        CAT_MUT_IN       => FUNCTION_PLAYER_MUT_IN,
        CAT_STOP         => FUNCTION_PLAYER_STOP,
        CAT_STAGE        => FUNCTION_INTERN,
        CAT_ADM          => FUNCTION_ADM,
        CAT_COMITY       => FUNCTION_COMITY,
        CAT_COMITY_SPORT => FUNCTION_COMITY_SPORT,
        CAT_DIR_TECH_G   => FUNCTION_TECH_DIR_MAXI,
        CAT_DIR_TECH_P   => FUNCTION_TECH_DIR_MINI,
        CAT_TECH_ADV     => FUNCTION_COMITY_TECH_ADV,
    ];

    $res = dbUtil()->selectRow("members", "num, category, category2, category3, category4,category5,category6, numEquip, numEquip2, numEquip2 where num<>0");
    while ($tup = dbUtil()->fetch_row($res)) {
      debugLog($tup);
      $insert = ["ri" => $tup[0]];
      for ($i = 1; $i <= 6; $i++) {
        if (!$tup[$i]) {
          continue;
        }
        $r = $tup[$i] & 0xFF0000;
        $numEquip = $tup[$i + 7];
        switch ($r) {
          case CAT_COACH:
            $set = ($team = $tup[$i] & 0xFFFF) % 10;
            $team = $this->getTeam($team) + $set;
            !$firstTeam && $firstTeam = $team;
            ($teams[FUNCTION_COACH][] = "-$team-") && (!$functions || array_search(FUNCTION_COACH, $functions) === false) && $functions[] = FUNCTION_COACH;
            break;

          case CAT_ASSIST:
            $set = ($team = $tup[$i] & 0xFFFF) % 10;
            $team = $this->getTeam($team) + $set;
            !$firstTeam && $firstTeam = $team;
            ($teams[FUNCTION_COACH_ASSIST][] = "-$team-") && (!$functions || array_search(FUNCTION_COACH_ASSIST, $functions) === false) && $functions[] = FUNCTION_COACH_ASSIST;
            break;

          case CAT_PLAYER:
            $set = ($team = $tup[$i] & 0xFFFF) % 10;
            $team = $this->getTeam($team) + $set;
            !$firstTeam && $firstTeam = $team;
            ($teams[FUNCTION_PLAYER][] = "-$team-") && (!$functions || array_search(FUNCTION_PLAYER, $functions) === false) && $functions[] = FUNCTION_PLAYER;
            $numEquips[] = $numEquip;
            break;

          default:
            $functions[] = $translate[$tup[$i]];
            break;
        }
      }
      !$functions && $functions[] = FUNCTION_PARENT;
      sort($functions);
      debugLog($functions, "functions");
      $teams && ksort($teams);
      debugLog($teams, "teams");
      $insert += [
          "function"  => $functions[0],
          "functions" => '-' . implode('-', $functions) . '-',
          "team"      => $firstTeam,
          "numEquip"  => $numEquips ? json_encode($numEquips) : null,
          "allTeams"  => $teams ? json_encode($teams) : null
      ];
      debugLog($insert, "insert");
      dbUtil()->insertRow("membersSeason", $insert);
      dbUtil()->query("update " . Login::get()->loginTable . " set roles='" . implode('-', ConfigProject::get()->getUserRoles($functions)) . "' where ri=$tup[0]");
      unset($functions);
      unset($teams);
      unset($numEquips);
      $team = null;
      $numEquip = null;
      $firstTeam = null;
    }

    $res = array_column(dbUtil()->fetch_all(dbUtil()->query("SELECT cellRef FROM `members` left join members_season as ms on ri=num group by cellRef having group_concat(ms.function) like '%"
                            . FUNCTION_PARENT . "%' and not group_concat(ms.function) like '%" . FUNCTION_PLAYER . "%' and not group_concat(ms.function) like '%" . FUNCTION_PLAYER_SUSP . "%'")), 0);
    debugLog($res);

    $res = dbUtil()->query("select functions, ri, function from members_season left join members on num=ri where " . dbUtil()->getSQLSelectAttrIn("cellRef", $res));
    while ($tup = dbUtil()->fetch_row($res)) {
      $tup[0] = str_replace('-' . FUNCTION_PARENT . '-', '-' . FUNCTION_OTHER . '-', $tup[0]);
      dbUtil()->updateRow("membersSeason", ($tup[2] == FUNCTION_PARENT ? ["function" => FUNCTION_OTHER] : []) + ["functions" => $tup[0]], "ri=$tup[1]");
    }
  }

  function getTeam($team)
  {
    $team >= 2650 && $team += 100;
    return intdiv($team, 10);
  }

}
