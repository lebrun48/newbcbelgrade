<?php

function buildMain()
{
  echo ""
  . "<main id='main'>";
  buildUsersBox();
  echo ""
  . "</main>";
}

function buildUsersBox()
{
  $table = new Table("users");
  echo ""
  . "<div id=usersBox class='card mt-3'>"
  . "  <div class=card-body>"
  . "    <h5 class='text-center card-title m-0'>Mise à jour des Utilisateurs</h5>";

  !utils()->isUserRole(ROLE_CASHIER) && utils()->isSmartphoneEngine && ($table->tableAttr = "class=table-responsive");
  $table->theadAttr = "class='grey lighten-4'";
  $table->defaultOrderLast = "name asc";
  $table->buildFullTable();

  echo ""
  . "  </div>"
  . "</div>";
  utils()->insertReadyFunction("usersListener");
}

class Table extends BuildTable
{

  function __construct($tableId, $content = null)
  {
    $this->colDefinition = [
        "name"     => [
            COL_TITLE      => "Prénom NOM",
            COL_DB         => "concat(firstName, ' ', upper($users.name))",
            COL_TD_ACTIONS => true,
        ],
        "mail"     => [COL_TITLE => "E-Mail"],
        "phoneNbr" => [COL_TITLE => "N° tél.", COL_TD_NO_ACTION => true],
        "prName"   => [
            COL_GROUP       => 0,
            COL_TD_ATTR     => "colspan=4 class=group-col",
            COL_GROUP_ORDER_DIRECTION => "desc",
            COL_DB          => "pr.name"
        ],
        "PRResp"   => [COL_HIDDEN => true, COL_DB => "pr.responsible"],
        "roles"    => [COL_TITLE => "Fonction(s)", COL_TD_ATTR => "class=text-nowrap"],
        "pr"       => [COL_HIDDEN => true, COL_EXT_REF => "pr"],
        "ri"       => [COL_HIDDEN => true, COL_PRIMARY => true, COL_DB => "$users.ri"]
    ];
    parent::__construct($tableId, $content);
  }

  function getTrAttributes($row = null)
  {
    return "class=text-nowrap";
  }

  function getRightsOnLine($row, $currentRights)
  {
    if (!utils()->hasRootRole() && !utils()->roles[ROLE_ADMIN_GLOBAL] && $row["pr"] != utils()->userSession()["pr"]) {
      return TABLE_RIGHT_ONLY_DISPLAY;
    }
    return parent::getRightsOnLine($row, $currentRights);
  }

  function getDisplayValue($key, $row)
  {
    switch ($key) {
      case "roles":
        $rights = $row[$key];
        for ($i = 1; $i < strlen($rights); $i++) {
          if ($rights[$i]) {
            switch ($i) {
              case ROLE_CASHIER:
                $display .= " + Ecritures Caisse";
                break;

              case ROLE_ADMIN_LOCAL:
                $display .= " + Administration PR";
                break;

              case ROLE_ADMIN_GLOBAL:
                $display .= " + Administration global";
                break;
            }
          }
        }
        return substr($display, 3);

      case "phoneNbr":

        return !$row[$key] ? '' : "<a style=color:blue;text-decoration:underline;text-decoration-color:blue  href='tel:" . $row[$key] . "'><i class='fas fa-phone mr-1'></i>" . $row[$key] . "</a>";

      case "name":
        return parent::getDisplayValue($key, $row) . ($row["ri"] == $row["PRResp"] ? "<span style=italic;color:rgb(240,20,20);margin-left:5px>(Resp.)</span>" : '');
    }
    return parent::getDisplayValue($key, $row);
  }

}
