<?php

class BillerStatementsSubmit extends BillerStatementsInsert
{
  function __construct()
  {
    $this->yearSeason = ConfigFinances::get()->yearSeason;
    parent::__construct();
  }

  function translateRow(BillerLine &$billerLine)
  {
    switch ($billerLine->account) {
      case 2: case 3: case 5: case 6: //FORTIS
        if (($i = stripos($billerLine->detail, " EXECUTE LE")) || ($i = stripos($billerLine->detail, " DATE VALEUR")))
          $billerLine->detail = substr($billerLine->detail, 0, $i);
        if (preg_match("/BE\d+|\d+/", $billerLine->orig)) {
          $billerLine->accountNb = $billerLine->orig;
          $billerLine->orig = '';
        }
        if ($i = preg_match("/(.*) (PAS DE COMMUNICATION|COMMUNICATION\s?:\s?(.*))/", $billerLine->detail, $matches)) {
          $billerLine->detail = $matches[3] ? $matches[3] : '';
          if (!$billerLine->orig) {
            $billerLine->orig = $matches[1];
            if (preg_match("/(.*) BE\d*/", $billerLine->orig, $matches))
              $billerLine->orig = $matches[1];
          }
        }
        else if (!$billerLine->orig) {
          $billerLine->orig = preg_match("/(.*) BE\d+/", $billerLine->detail, $matches) ? $matches[1] : $billerLine->detail;
          $billerLine->detail = '';
        }
        return;

      case 52: //ING
      case 53:
        do
          $billerLine->orig = str_replace('  ', ' ', $billerLine->orig, $n);
        while ($n);
        do
          $billerLine->detail = str_replace('  ', ' ', $billerLine->detail, $n);
        while ($n);
        if (strpos($billerLine->orig, "Décompte de frais") === 0) {
          $billerLine->orig = strtok($billerLine->orig, "+-");
          return;
        }

        strtok($billerLine->orig, "+-");
        $billerLine->orig = strtok(null);
        $billerLine->orig = substr($billerLine->orig, strpos($billerLine->orig, ' ', 1) + 1);
        $i = strpos($billerLine->orig, " Communication:");
        if ($i !== false)
          $billerLine->orig = substr($billerLine->orig, 0, $i);
        $i = strpos($billerLine->orig, ",");
        if ($i !== false)
          $billerLine->orig = substr($billerLine->orig, 0, $i);
        $billerLine->orig = str_replace("Vers: ", '', $billerLine->orig);
        $billerLine->orig = str_replace("De: ", '', $billerLine->orig);
        return;
    }
  }

}
