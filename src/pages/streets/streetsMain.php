<?php

function buildMain()
{
  echo ""
  . "<main id='main'>";
  new StreetsBox;
  echo ""
  . "</main>";
}

class StreetsBox
{

  public function __construct()
  {
    $table = new Streets("streets");
    echo ""
    . "<div id=streetsBox class='container mt-3'>";

    $table->settings[TABLE_ATTR_FAST_SEARCH_ENABLE] = true;
    $table->buildFullTable();

    echo ""
    . "</div>";
    utils()->insertReadyFunction("tableListener", "streets");
  }

}

class Streets extends BuildTable
{

  static function buildTableDefs()
  {
    $cityTable = dbUtil()->getTableName("cities");
    dbUtil()->tables["streets"][DB_COLS_DEFINITION] = [
        "id"         => [COL_TITLE => "Id", COL_PRIMARY => true, COL_TD_ATTR => "class=td-1", COL_TD_ACTIONS => true],
        "name"       => [COL_TITLE => "Nom"],
        "city"       => [COL_TITLE => "Commune", COL_EXT_REF => ["cities a", null, "ri"], COL_DB => "a.name"],
        "zip"        => [COL_TITLE => "CP", COL_DB => "a.zipCode"],
        "cityRI"     => [COL_TITLE => "cityRI", COL_DB => "city"],
        "mainCity"   => [COL_TITLE => "principal", COL_EXT_REF => "cities b.ri", COL_DB => "b.name"],
        "zipMain"    => [COL_TITLE => "CP principal", COL_DB => "b.zipCode"],
        "mainCityRI" => [COL_TITLE => "MainCityRI", COL_DB => "b.ri"],
    ];
  }

  function __construct($tableId, $content = null)
  {
    $this::buildTableDefs();
    parent::__construct($tableId, $content);
  }

  protected function getDisplayValue($key, $row)
  {
    return parent::getDisplayValue($key, $row);
  }

  protected function getLimit()
  {
    return 200;
  }

}

function axStreetsEdit()
{
  switch (utils()->action) {
    case "update" :
    case "insert" :
      new StreetsEdit();
      return;

    case "delete":
      msgBox("<p>Confirmation suppression</p>", "Suppression", MODAL_SIZE_SMALL, [MSGBOX_BUTTON_ACTION => "Supprimer", MSGBOX_BUTTON_CLOSE => "Annuler"]);
      return;
  }
}

function axStreetsSubmit()
{
  switch (utils()->action) {
    case "update" :
      !$_REQUEST["val_isMain"] && $_REQUEST["val_isMain"] = "=null";
  }
  utils()->axRefreshElement("streets");
}
