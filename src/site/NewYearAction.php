<?php

class NewYearAction
{

  public function __construct($newYear)
  {
    //members Season
    dbUtil()->seeRequest = 1;
    $prevYear = $newYear - 1;
    $membersSeason = dbUtil()->getTableNameNoAs("membersSeason");
    dbUtil()->begin_transaction();
    dbUtil()->query("insert into $membersSeason(ri, season, team, numEquip, allTeams, function, functions) "
            . "select ri, $newYear, team, null, allTeams, function, functions from $membersSeason where season=$prevYear");
    dbUtil()->deleteRow("membersSeason", "season<$prevYear", false);
    new FinancesNewYearAction($newYear);
    //dbUtil()->commit();
  }

}
