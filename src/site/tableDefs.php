<?php

include "common/src/tableDef.php";
include "common/src/buildCompos.php";

function buildTableDefs()
{
//users tableId
  Login::get();
  dbUtil()->tables["calendar"] = [
      DB_TABLENAME    => "calendar",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL
  ];

  dbUtil()->tables["biller"] = [
      DB_TABLENAME    => "billerNBCB biller",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL | TABLE_RIGHT_USER_ACTION
  ];

  dbUtil()->tables["budget"] = [
      DB_TABLENAME    => "billerNBCB_budget budget",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL,
      DB_DEFAULT_INSERT => "season=" . ConfigFinances::get()->yearSeason,
  ];

  dbUtil()->tables["billerClassify"] = [
      DB_TABLENAME    => "billerNBCB_classify",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL
  ];

  dbUtil()->tables["billerNum"] = [
      DB_TABLENAME    => "billerNBCB_num",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL
  ];
  dbUtil()->tables["streets"] = [
      DB_TABLENAME    => "streets street",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL
  ];
  dbUtil()->tables["cities"] = [
      DB_TABLENAME    => "cities city",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL
  ];
  dbUtil()->tables["cells"] = [
      DB_TABLENAME    => "cells cell",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL
  ];
  dbUtil()->tables["members"] = [
      DB_TABLENAME    => "members mb",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL | TABLE_RIGHT_USER_ACTION,
  ];
  dbUtil()->tables["membersSeason"] = [
      DB_TABLENAME      => "members_season ms",
      DB_DEFAULT_INSERT => "season=" . ConfigFinances::get()->yearSeason,
      DB_DEFAULT_WHERE  => "ms.season=" . $_REQUEST["year"],
      DB_RIGHTS_ACCES   => TABLE_RIGHT_ALL
  ];
  dbUtil()->tables["category"] = [
      DB_TABLENAME    => "category",
      DB_RIGHTS_ACCES => TABLE_RIGHT_ALL
  ];
}

//    dbUtil()->tables["members"][DB_COLS_DEFINITION] = [
//        "num"      => [
//            COL_PRIMARY => true,
//        //COL_HIDDEN  => true
//        ],
//        "name"     => [
//            COL_TITLE => "Nom Prénom",
//            COL_DB    => "concat($tb.name, ' ',firstName)"
//        ],
//        "sex"      => [
//            COL_TITLE => "Genre",
//        ],
//        "sz"       => [
//            COL_TITLE => "taille",
//        ],
//        "function" => [
//            COL_TITLE   => "Fonction",
//            COL_EXT_REF => ["membersSeason ms", "ri", "num"],
//            COL_DB      => "ms.roles"
//        ],
//        "team"     => [
//            COL_TITLE   => "Equipe",
//            COL_EXT_REF => ["category c", "num", "(ms.team div 10)*10"],
//            COL_DB      => "concat(c.short, ' ', if(ms.team%10, char(65 + (ms.team%10)), ''))"
//        ],
//        "email"    => [
//            COL_TITLE => "EMail",
//        ],
//        "phone"    => [
//            COL_TITLE => "GSM",
//        ],
//        "emailF"   => [
//            COL_TITLE => "EMail1",
//        ],
//        "phF"   => [
//            COL_TITLE => "ph1",
//        ],
//        "emailM"   => [
//            COL_TITLE => "EMail2",
//        ],
//        "phM"   => [
//            COL_TITLE => "ph2",
//        ],
//        "streetId" => [
//            COL_HIDDEN  => true,
//            COL_TITLE   => "N°",
//            COL_EXT_REF => ["cells cell", "ri", "cellRef"],
//            COL_DB      => "if(cellRef, concat(cell.buildingNb,if(cell.box, concat(' / ', cell.box),'')),null)"
//        ],
//        "address"  => [
//            COL_HIDDEN  => true,
//            COL_TITLE   => "Adresse",
//            COL_EXT_REF => ["streets s", "id", "cell.streetID"],
//            COL_DB      => "s.name"
//        ],
//        "city"     => [
//            COL_HIDDEN  => true,
//            COL_TITLE   => "Ville",
//            COL_EXT_REF => ["cities city", "ri", "s.city"],
//            COL_DB      => "city.name"
//        ],
//        "cellRef"  => [
//            COL_GROUP   => 0,
//            COL_TD_ATTR => "colspan=4 class=group-col",
//        ],
//        "birth"    => [
//            COL_TITLE => "Naissance",
//        ],
//        "dateIn"   => [
//            COL_TITLE => "Entrée",
//        ],
//    ];
