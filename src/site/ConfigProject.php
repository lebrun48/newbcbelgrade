<?php

define("URL_CLUB", "http://www.newbcbelgrade.be");
define("URI_CLUB", $isLocalHostForTest ? "localhost/bcbelgrade" : "www.newbcbelgrade.be");
define("KEY_CLUB", "belgrade");
define("MATRICULE_CLUB", "1702");
define("NAME_CLUB", "SAN Mazuin BC Namur-Belgrade");
define("SHORT_NAME_CLUB", "NBCAB");
define("TVA_CLUB", "BE 459.190.278");
define("STREET_CLUB", "Rue J. Vincent,76");
define("ZIP_CODE_CLUB", "5001");
define("CITY_CLUB", "Belgrade");
define("ADDRESS_CLUB", STREET_CLUB . " " . ZIP_CODE_CLUB . " " . CITY_CLUB);
define("TREASURER_NAME_CLUB", "Cédric JUSTE");
define("TREASURER_MAIL_CLUB", "cedric_juste@hotmail.com");
define("TREASURER_PHONE_CLUB", "0474/726124");
define("LOGO_CLUB", "__NEW__logo_6x6.png");
define("LOGO_LITTLE_CLUB", "__NEW__logo_3x3.png");
define("PHONE_CLUB", "0497/41 02 67");
define("EMAIL_CLUB", "bcb1702@skynet.be");
define("ACCOUNT_CLUB", "BELFIUS: BE67 0682 2338 7387");

define('AFFILIATION_NONE', 0);
define('AFFILIATION_CLUB', 1);
define('AFFILIATION_OTHER', 2);
define('AFFILIATION_TO_AFFILIATE', 3);
define('AFFILIATION_TO_REMOVE', 4);

//functions
define('FUNCTION_PARENT', 24);

define('TEAM_MAX', 325);
define('TEAM_SENIOR', 135);

class Enterprise
{

  public $name = NAME_CLUB;
  public $num = MATRICULE_CLUB;
  public $numTVA = TVA_CLUB;
  public $address = ADDRESS_CLUB;
  public $financesResponsible = TREASURER_NAME_CLUB;
  public $logo = LOGO_CLUB;

}

class ConfigProject extends ConfigBase
{

  public $affiliationType = ["Non affilié", "Club", "Autre club", "A affilié", "A désaffilier"];
  private $enterprise;
  private $sizes;
  private $functions;
  private $categories;
  private $usedTeams;
  private $roles;

  public function __construct()
  {
    $this->roles = [
        ROLE_ADM       => "Administrateur",
        ROLE_SECRETARY => "Secrétaire",
        ROLE_TREASURER => "Trésorier",
        ROLE_PLAYER    => "Joueur",
        ROLE_PARENT    => "Parent",
        ROLE_COACH     => "Entraineur / coach",
        ROLE_OTHER     => "Autre"
    ];
    parent::__construct();
  }

  private function setCurrent($keyIdx)
  {
    $this->currentIdx = $keyIdx;
    switch ($keyIdx) {
      case CONFIG_MEMBERS_CATEGORIES:
        $this->currentAttr = &$this->categories;
        break;

      case CONFIG_MEMBERS_FUNCTIONS:
        $this->currentAttr = &$this->functions;
        break;

      case CONFIG_MEMBERS_SIZES:
        $this->currentAttr = &$this->sizes;
        break;

      default:
        throw new Exception("Invalid keyIdx");
    }
    $this->buildAttr();
  }

  public function getRoles()
  {
    return $this->roles;
  }

  public function getUserRoles($functions)
  {
    $this->setCurrent(CONFIG_MEMBERS_FUNCTIONS);
    is_string($functions) && $functions = explode('-', $functions);
    foreach ($functions as $function) {
      $ar[] = $this->getPropById($function, "role");
    }
    return array_unique($ar);
  }

  function hasRole($functions, $role)
  {
    $this->setCurrent(CONFIG_MEMBERS_FUNCTIONS);
    is_string($functions) && $functions = explode("-", substr($functions, 1, -1));
    !is_array($role) && $role = [$role];
    if ($functions) {
      foreach ($functions as $function) {
        if (array_search($this->getPropById($function, "role"), $role) !== false) {
          return true;
        }
      }
    }
  }

  function getFunctionsForRole($roles)
  {
    $this->setCurrent(CONFIG_MEMBERS_FUNCTIONS);
    !is_array($roles) && $roles = [$roles];
    foreach ($this->getList() as $index => $val) {
      array_search($val["role"], $roles) !== false && $ret[] = $val["id"];
    }
    return $ret;
  }

  function getFunctionsName($functions)
  {
    $this->setCurrent(CONFIG_MEMBERS_FUNCTIONS);
    if (is_numeric($functions)) {
      return $this->getNameById($functions);
    }
    foreach ($functions as $i) {
      $value .= ", " . $this->getNameById($i);
    }
    return substr($value, 2);
  }

  function getFunctionRole($function)
  {
    $this->setCurrent(CONFIG_MEMBERS_FUNCTIONS);
    if (is_numeric($function)) {
      return $this->getPropById($function, "role");
    }
    foreach ($function as $i) {
      $value[] = $this->getPropById($i, "role");
    }
    return $value;
  }

  function getPicturePathMember($num)
  {
    return serviceConfig()->config["path"]["picture_path"] . "/N_$num.jpg";
  }

  function getUsedTeams()
  {
    !$this->usedTeams && $this->buildUsedTeams();
    return $this->usedTeams;
  }

  function getUsedTeamsName()
  {
    foreach ($this->getUsedTeams() as $team) {
      $ar[$team] = $this->getTeamShortName($team);
    }
    return $ar;
  }

  private function buildUsedTeams()
  {
    $members = dbUtil()->getTableName("members");
    $membersSeason = dbUtil()->getTableName("membersSeason");
    $this->usedTeams = array_column(dbUtil()->fetch_all(dbUtil()->query("select team from $members left join $membersSeason on ri=num where ms.season=" . ConfigFinances::get()->yearSeason . " group by team having team is not null")), 0);
  }

  function getTeamShortName($teams): string
  {
    $this->setCurrent(CONFIG_MEMBERS_CATEGORIES);
    if (is_array($teams)) {
      foreach ($teams as $team) {
        $ret .= ", " . $this->getPropById(intdiv($team, 10), "short") . (($set = $team % 10) ? " " . chr(64 + $set) : '');
      }
      return substr($ret, 2);
    }
    return $this->getPropById(intdiv($teams, 10), "short") . (($set = $teams % 10) ? " " . chr(64 + $set) : '');
  }

  function getEnterprise(): Enterprise
  {
    !$this->enterprise && $this->enterprise = new Enterprise;
    return $this->enterprise;
  }

  static function get($keyIdx = null): ConfigProject
  {
    static $config;
    !$config && $config = new ConfigProject();
    isset($keyIdx) && $config->setCurrent($keyIdx);
    return $config;
  }

}
