<?php

$include_path = ""
        . PATH_SEPARATOR . serviceConfig()->config["path"]["project_full_path"] . '/' . serviceConfig()->config["path"]["sources_path"] . "/site"
        . PATH_SEPARATOR . serviceConfig()->config["path"]["project_full_path"] . "/plugin"
        . PATH_SEPARATOR . (($tmp = serviceConfig()->config["path"]["plugin_path"])[0] == '/' ? serviceConfig()->config["path"]["include_path"] . "$tmp" : "$srv/$tmp");

set_include_path(get_include_path() . $include_path);

include "defines.php";

include "main.php";
define('NL', "<br>");
define('nl', "\n");

define('CONFIG_MEMBERS_CATEGORIES', 1);
define('CONFIG_MEMBERS_FUNCTIONS', 2);
define('CONFIG_MEMBERS_SIZES', 3);
define('CONFIG_FINANCES_CATEGORIES', 4);
define('CONFIG_FINANCES_SEASON', 5);

define('CONFIG_NEW_SEASON_DATE', "07-01");


include "common/src/Utils.php";
Login::get()->loginOptions = LOGIN_OPTION_2_PHASES | LOGIN_OPTION_REMEMBER_ME;
utils()->projectName = SITE_PROJECT;
utils()->checkRequest();

