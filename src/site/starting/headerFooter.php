<?php

define("MENU_FINANCE", 'finances');
define("MENU_BILLER", "biller");
define("MENU_LOGOUT", "logout");

function buildHeader()
{
  echo ""
  . "  <h4 style=overflow-x:auto class='h4 text-center text-nowrap my-lg-3'>" . ConfigProject::get()->getEnterprise()->name . "</h4>"
  . "  <p class='my-0 text-right' style=font-size:15px class=ml-2>(" . utils()->userSession()["firstName"] . " " . strtoupper(utils()->userSession()["name"]) . ")</p>";
  buildMenu();
}

include "common/src/buildMenu.php";

function buildMenu()
{
  $menu = [
      NAV_POSITION   => NAV_POSITION_STICKY_TOP,
      NAV_EXPANDABLE => NAV_EXPANDABLE_SM,
      NAV_ATTR       => "class='" . SITE_DEFAULT_COLOR . " navbar-dark'",
      NAV_LINK       =>
      [
          MENU_FINANCE => [
              NAV_LINK_ICON    => "<i class='fas fa-euro-sign mr-1'></i>",
              NAV_LINK_HIDE_ON => "md",
              NAV_LINK_TITLE   => "Finances",
              NAV_LINK_ATTR    => "href=?setPage=finances",
          ],
          [
              NAV_LINK_ICON    => "<i class='fas fa-users mr-1'></i>",
              NAV_LINK_HIDE_ON => "md",
              NAV_LINK_TITLE   => "Membres",
              NAV_LINK_ATTR    => "href=?setPage=members",
          ],
          [
              NAV_LINK_ICON    => "<i class='fas fa-basketball-ball mr-1'></i>",
              NAV_LINK_HIDE_ON => "md",
              NAV_LINK_TITLE   => "Sportif",
              NAV_LINK_ATTR    => "onclick=load('sports')",
          ],
          [
              NAV_LINK_TITLE   => "Tools",
              NAV_LINK_ATTR    => "href=?setPage=tools",
          ],
          [
              NAV_LINK_TITLE   => "Streets",
              NAV_LINK_ATTR    => "href=?setPage=streets",
          ],
          [
              NAV_LINK_TITLE   => "Cities",
              NAV_LINK_ATTR    => "href=?setPage=cities",
          ],
      ],
      NAV_LINK_RIGHT =>
      [
          MENU_LOGOUT => [
              NAV_LINK_TITLE   => "Déconnexion",
              NAV_LINK_HIDE_ON => "sm",
              NAV_LINK_ICON    => '<i class="fas fa-sign-out-alt mr-1"></i>',
              NAV_LINK_ATTR    => "onclick = axExecute('logout')"
          ]
      ]
  ];

  $toRemove = [];
//  if (!utils()->hasRootRole()) {
//    (utils()->isUserRole(ROLE_CASHIER) || utils()->roles[ROLE_ADMIN_LOCAL]) && ($toRemove = [MENU_PR]);
//    !utils()->isLocalTest && !utils()->isLogAs && !utils()->isDemo && ($toRemove = array_merge($toRemove, [MENU_LOGOUT]));
//    !utils()->hasUserRole(ROLE_CASHIER) && utils()->hasUserRole(ROLE_ADMIN_GLOBAL) && ($toRemove = array_merge($toRemove, [MENU_CASH]));
//  }


  buildNavbar($menu, $toRemove);
  utils()->insertReadyFunction("menuListener");
}
