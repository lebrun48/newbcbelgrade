<?php

include "htmlPage.php";

echo "<body>";
echo "<div id=debug_info></div><div id=modals></div>";
echo ""
 . "<div style='position: fixed;bottom:50px;left:20px' class='font-size toast special-color text-white' data-delay=5000>"
 . "  <div class='toast-body' style=font-size:15px>"
 . "  </div>"
 . "</div>";

if (!utils()->hasSession()) {
  Login::get()->userLogin("Connexion BC Belgrade", utils()->imgPath . "/logo.png", "green darken-2");
}
else {
  utils()->hasUserRole(ROLE_SECRETARY) && ConfigProject::get()->executeYearStatusChange();
  echo "<div class='container-fluid'>";
  include "headerFooter.php";
  if (function_exists("buildHeader")) {
    buildHeader();
  }
  include utils()->getCurrentPageName() . "Main.php";
  buildMain();

  if (function_exists("buildFooter")) {
    buildFooter();
  }
  utils()->insertIncludeScripts("page");
  echo "</div>";
}

include "scriptPage.php";
echo "</body>";

