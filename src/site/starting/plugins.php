<?php

include "mdb_4.19/loadPlugin.php";
include "numeral/loadPlugin.php";
include "finances/loadPlugin.php";
include "common/loadPlugin.php";
include "mailer/loadPlugin.php";
include "moment/loadPlugin.php";
include "dt_picker/loadPlugin.php";
include "fpdf/loadPlugin.php";
Plugin::registerAutoLoad("finances");