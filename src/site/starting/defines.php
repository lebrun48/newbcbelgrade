<?php

include "setRights.php";

//site specific
define('SITE_PROJECT', "bcbelgrade");
define('SITE_NO_DEMO', true);

//Colors
define('SITE_DEFAULT_COLOR', "green darken-2");
define ('SITE_DEFAULT_NB_COLOR', "#388e3c");
define('SITE_DEFAULT_TEXT_COLOR', "green");
define('SITE_DEFAULT_BUTTON_COLOR', "btn-dark-green");
