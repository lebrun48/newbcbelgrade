<?php

class ConfigFinances extends ConfigFinancesBase
{

  private $enterprise;
  public $account = [
      5   => ["Fintro CC", true],
      6   => ["Fintro Ep", true],
      0   => ["Belfius CC", true],
      1   => ["Belfius Ep", true],
      7   => ["ING CC", true],
      2   => ["(Belfius I)", false],
      3   => ["(Fintro CC)", false],
      4   => ["(Fintro Ep)", false],
      9   => ["Bons d'achats", false],
      100 => ["Caisse", true],
      101 => ["Caisse ESB", false],
      200 => ["Patrimoine", true],
      201 => ["Infos", true],
      202 => ["Compte TVA", true],
      300 => ["Caisse +", true],
      301 => ["DB +", false],
      302 => ["Banque+", true],
      305 => ["Coffre+", false],
      304 => ["C. Boutique", true],
      400 => ["Compte report"],
  ];
//nméro de séquence	Date d'exécution	Date valeur	Montant	Devise du compte	CONTREPARTIE DE LA TRANSACTION	Détails	Numéro de compte
//        0               1                 2        3       4                          5                       6       7
  public $IBAN_Account = array("BE88143101737341" => 5, "BE47144890236980" => 6); //translate the IBAN account into index account in $account attribute;
  public $numAccount = [
      //En-tête   ,account,date,tiers,detail,montant+,Montant-,N° compte
      ["file", "Num", -7, 1, 5, 6, 3, -1, -1], //account <0 indicates the colonne nb of the concerned IBAN account 
      ["BE67 0682 2338 7387", "Compte;", 0, 1, 5, 14, 10, -1, 4],
      ["BE09 0882 0989 4857", "Compte;", 1, 1, 5, 14, 10, -1, -1],
      ["BE40 0835 9289 9163", "Compte;", 2, 1, 5, 14, 10, -1, -1],
      ["BE85142402363806", "\"ANNEE +", 3, 1, 6, 10, 3, -1, -1],
      ["BE30143853416411", "\"ANNEE +", 4, 1, 6, 10, 3, -1, -1],
      ["377-0379617-93", "Numéro", 52, 4, 8, 9, 6, -1, -1],
      ["ESB", "Tiers", 11, 1, 0, 4, 5, 2, -1]
  ];

  public function __construct()
  {
    // utils()->now = new DateTime("2023-01-02");
    $this->isPrivate = false;
    $this->hasBudget = true;
    parent::__construct();
  }

  function getEnterprise(): Enterprise
  {
    return $this->enterprise;
  }

}
