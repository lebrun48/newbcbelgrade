
function menuListener()
{

}


function showToast(text)
{
  $(".toast .toast-body").text(text);
  $(".toast").toast("show");
}

function membersHeaderListener()
{
  optionsDropdownListener();
}

paramConfigMembers = {categories: "Catégories", functions: "Fonctions", roles: "Rôles", sizes: "Tailles"};

function activateMembers(active, toggleParam)
{
  $("#main [name='active']").val(active);
  if (typeof toggleParam === "boolean") {
    //config tag
    $("#main [name='paramShowed']").val(toggleParam ? active : '');
    if (toggleParam) {
      //show config
      ($navs = $("#main .nav-tabs")).find("a")
              .removeClass("active")
              .last().addClass("active").children().first().attr("onclick", "activateMembers('" + active + "',true)").text(paramConfigMembers[active]);
      $navs.children().last().show(300);
      //show tab config
      $(".main-shown").hide(200);
      pageAction("configTab");
      return;
    }
  }
  pageAction("members");

}

function ed_cityListener()
{
  $("#ed_city").change(function () {
    axExecute("edit", {table: 'members', action: "resetAddress"}, function (data) {
      $("#ed_address").materialSelect({destroy: true});
      $("#ed_address").parent().replaceWith(data);
      $("#ed_address").materialSelect();
      searchSelectListener("members", ["ed_address"]);
    })
    $("#ed_buildingNb").removeAttr("disabled").val(null).next().removeClass("active");
    $("#ed_box").removeAttr("disabled").val(null).next().removeClass("active");
  });
}



function membersFormListener()
{
  const ROLE_PLAYER = 4;
  const ROLE_PARENT = 5;
  const ROLE_COACH = 6;
  const TEAM_SENIOR = 13;
  const AFFILIATION_CLUB = 1;
  const AFFILIATION_TO_AFFILIATE = 3;

  function updateFields()
  {
    ed_affiliation = $("#ed_affiliation").val();
    isParent = isPlayer = false;
    isSenior = false;
    maxTeam = 0;
    isOnlyParent = 0;
    hasFunction = false;
    $("#membersForm [name^='func']").each(function () {
      //role parent and/or player
      role = $(this).children().eq($(this).prevAll("ul").children(".active").index() - 1).data("role");
      !isParent && (isParent = role == ROLE_PARENT) && ++isOnlyParent || (isOnlyParent = 2);
      !isPlayer && (isPlayer = role == ROLE_PLAYER);
      hasFunction = $(this).val();
      role == ROLE_PLAYER && (team = $("#membersForm [name='team" + this.name.slice(-1) + "']").val()) && (maxTeam = Math.max(maxTeam, team));
    });
    isOnlyParent = isOnlyParent == 1;
    maxTeam && (isSenior = maxTeam <= TEAM_SENIOR);
    //affiliation est 'club'(1) ou 'à affilier'(3)
    setMandatory("#ed_email", isParent);
    setMandatory("#ed_name", ed_affiliation == AFFILIATION_CLUB || ed_affiliation == AFFILIATION_TO_AFFILIATE || hasFunction && !isOnlyParent);
    setMandatory("#ed_firstName", ed_affiliation == AFFILIATION_CLUB || ed_affiliation == AFFILIATION_TO_AFFILIATE || hasFunction && !isOnlyParent);
    setMandatory("#ed_sex", ed_affiliation == AFFILIATION_CLUB || ed_affiliation == AFFILIATION_TO_AFFILIATE);
    setMandatory("#ed_birth", ed_affiliation == AFFILIATION_CLUB || ed_affiliation == AFFILIATION_TO_AFFILIATE);
    setMandatory("#ed_city", $("#ed_city").val() || ed_affiliation == 1 || ed_affiliation == AFFILIATION_TO_AFFILIATE);
    //parents show or not
    setMandatory("#ed_email1", isPlayer && !isSenior);
    if (isPlayer) {
      $("#membersForm .parent-nav").show(200);
      if ($("#membersForm [name='isInsert']").val()) {
        ($affiliation = $("#ed_affiliation")).val() === '0' && selectClickByValue($affiliation, '');
        $affiliation.prevAll("ul").children().eq(1).hide();
      }
    } else {
      $("#membersForm .parent-nav").hide(200)
      $("#ed_affiliation").prevAll("ul").children().eq(1).show();
    }
  }


  $("#ed_affiliation").change(function () {
    updateFields();
  });
  updateFields();


  ed_cityListener();

  function functionClickListener($dom)
  {
    ($remove = $("#membersForm .function-remove")).length == 1 && $remove.addClass("disabled");
    //remove icon
    $dom.find(".function-remove").click(function () {
      $("#membersForm .function-remove").length > 1 && $(this).parents(".form-row").hide(200, function () {
        $(this).remove();
        updateFields();
      });
    })

    //add icon
    $dom.find(".function-add").click(function (event) {
      ret = false
      $("#membersForm [name^='func']").each(function () {
        if (!$(this).val()) {
          ret = true;
          return false;
        }
      })
      if (ret) {
        return;
      }

      id = 0;
      $("#membersForm [name^='func']").each(function () {
        id = Math.max(this.name.slice(4), id);
      })
      axExecute("edit", {page: "members", action: "getNewRow", id: id + 1}, function (data) {
        $(event.target).parents(".form-row").after(data).next().find(".mdb-select").materialSelect();
        functionClickListener($(event.target).parents(".form-row").next());
      })

    })

    //function change
    $dom.find("[name^='func']").change(function () {
      //team or not
      $toShow_hide = $(this).parents("[class^='col-']").nextAll("[class^='col-']");
      (t = $(this).children("[value='" + $(this).val() + "']").data("role")) == ROLE_PLAYER || t == ROLE_COACH ? $toShow_hide.show(300) : $toShow_hide.hide(300);
      $(this).parents(".form-row").find(".function-add").removeClass("disabled");
      updateFields();
    });
    $dom.find("[name^='func']").change();

    //team change
    $dom.find("[name^='team']").change(updateFields);

  }

  functionClickListener($("#membersForm"));
}

function membersFormValidate()
{
  if ((birth = $("#ed_birth")).length && (birth = birth.get(0))) {
    $dt = birth.value.split("/");
    diff = (new Date()) - (new Date()).setFullYear($dt[2], $dt[1], $dt[0]);
    if ((diff = Math.round(diff / 1000 / 3600 / 24 / 365)) < 3 || diff > 100) {
      birth.setCustomValidity("L'âge doit être compris entre 3 et 100 ans");
      return;
    }
    birth.setCustomValidity('');
    if (diff >= 18 && !$("#ed_email").val()) {
      showToast("l'adresse mail doit être demandée (+ de 18 ans)");
    }
  }
}

function membersFormInvalid()
{
  $("#membersForm .nav-tabs a").removeClass("text-danger");
  $("#membersForm input:invalid").each(function () {
    (id = $(this).parents(".tab-pane").attr("id")) && $("#membersForm .nav-tabs a[href='#" + id + "']").addClass("text-danger");
  });
}

function continueLoad(offset, i, action)
{
  location.replace("?offset=" + offset + "&i=" + i + "&action=" + action);
}




